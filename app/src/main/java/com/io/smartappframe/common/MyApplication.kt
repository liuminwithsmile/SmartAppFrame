package com.io.smartappframe.common

import android.app.Application
import com.io.thirdparties.aliyun.AliyunOSSHelper
import leakcanary.LeakCanary

class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        AliyunOSSHelper.initAliOSS(applicationContext, 10)
    }
}
