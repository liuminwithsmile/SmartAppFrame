package com.io.smartappframe.common

/**
 * Description:常量
 * 2021-04-02 10:17
 * @author LiuMin
 */
object AppConstants {
    val TAKE_PHOTO_REQUEST_CODE = 100 //拍照的请求码
    val PHOTO_ALBUM_REQUEST_CODE = 101 //拍照的请求码
    val ALIYUN_OBJECT_KEY = "IOAndroidTest/"
    val Aliyun_Path = "http://soonsmart.keji.io/"
}