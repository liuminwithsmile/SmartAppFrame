package com.io.smartappframe.mvp.presenter

import android.content.Context
import android.content.Intent
import com.io.base.mvp.BasePresenter
import com.io.smartappframe.mvc.activity.AMapLocationActivity
import com.io.smartappframe.mvp.contract.ThirdPartiesMainContract
import com.io.smartappframe.mvp.model.ThirdPartiesMainModel
import com.io.smartappframe.thirdparties.zxing.QRCodeActivity

/**
 * Description:
 * 2021-04-16 10:33
 * @author LiuMin
 */
class ThirdPartiesMainPresenter(view: ThirdPartiesMainContract.IMainView) :
    BasePresenter<ThirdPartiesMainContract.IMainModel, ThirdPartiesMainContract.IMainView>() {
    private var model:ThirdPartiesMainContract.IMainModel
    private var mainView:ThirdPartiesMainContract.IMainView
    init {
        model = ThirdPartiesMainModel()
        mainView = view
    }

    /**
     * 获取数据列表并显示
     */
    fun getFunctionList() {
        val list = model.produceData()
        list?.let {
            if (it.size > 0) {
                mainView.showSuccess(it)
            } else {
                mainView.showFail("数据为空")
            }
        } ?: let {
            mainView.showFail("数据为空")
        }
    }

    /**
     * 根据function的id决定进入哪个页面
     */
    fun goOtherPage(id: Int, context: Context) {
        when (id) {
            0 -> {
                context.startActivity(
                    Intent(
                        context,
                        AMapLocationActivity::class.java
                    )
                )
            }
            1 -> {
                context.startActivity(
                    Intent(
                        context,
                        QRCodeActivity::class.java
                    )
                )
            }
        }
    }
}