package com.io.smartappframe.mvp.model

import com.io.base.mvp.BaseModel
import com.io.smartappframe.mvp.contract.CommonLinkMainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:通信工具测试类主页Model
 * 2021-03-24 10:46
 * @author LiuMin
 */
class CommonLinkMainModel: CommonLinkMainContract.IMainModel, BaseModel() {
    override fun produceData(): MutableList<FunctionItem>? {
        val list = mutableListOf<FunctionItem>()
        var item = FunctionItem()
        item.id = 0
        item.name = "HTTP"
        list.add(item)

        item = FunctionItem()
        item.id = 1
        item.name = "MQTT"
        list.add(item)

        item = FunctionItem()
        item.id = 2
        item.name = "串口通信"
        list.add(item)

        item = FunctionItem()
        item.id = 3
        item.name = "局域网通信"
        list.add(item)
        return list
    }
}