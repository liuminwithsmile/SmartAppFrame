package com.io.smartappframe.mvp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.io.base.mvp.BaseAdapter
import com.io.base.mvp.BaseHolder
import com.io.smartappframe.databinding.ItemSimpleFriendBinding
import com.io.smartappframe.mvp.model.entity.Friend

/**
 * Description:网格布局列表的Adapter
 * 2021-03-19 16:15
 * @author LiuMin
 */
class GridRecyclerViewAdapter: BaseAdapter<Friend>() {
    override fun getViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ViewBinding {
        return ItemSimpleFriendBinding.inflate(inflater, parent, false)
    }

    override fun setData(
        holder: BaseHolder,
        viewBinding: ViewBinding,
        data: Friend,
        position: Int
    ) {
        val binding = viewBinding as ItemSimpleFriendBinding
        binding.tvId.setText(data.id.toString())
        binding.tvName.setText(data.name)
    }
}