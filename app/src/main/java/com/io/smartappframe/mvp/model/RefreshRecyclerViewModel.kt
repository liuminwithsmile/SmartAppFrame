package com.io.smartappframe.mvp.model

import com.io.base.mvp.BaseModel
import com.io.commonlib.helper.LogHelper
import com.io.smartappframe.mvp.contract.RefreshRecyclerViewContract
import com.io.smartappframe.mvp.model.entity.Friend

/**
 * Description:带有刷新功能的RecyclerView的Model
 * 2021-03-15 18:24
 * @author LiuMin
 */
class RefreshRecyclerViewModel : RefreshRecyclerViewContract.IMainModel, BaseModel() {
    private val TAG = RefreshRecyclerViewModel::class.java.simpleName
    override fun produceData(startIndex: Int, length:Int): MutableList<Friend>? {
        LogHelper.d(TAG, "produceData--startIndex：${startIndex}")
        val list = mutableListOf<Friend>()
        for (i in startIndex..(startIndex + length - 1)) {
            var friend = Friend()
            friend.id = i
            friend.name = "无名$i"
            friend.jobTitle = "职称$i"
            list.add(friend)
        }
        return list
    }

}