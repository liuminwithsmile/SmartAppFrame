package com.io.smartappframe.mvp.model

import com.io.base.mvp.BaseModel
import com.io.smartappframe.mvp.contract.MainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:展示功能列表的Model
 * 2021-03-03 17:58
 * @author LiuMin
 */
class MainModel : MainContract.IMainModel, BaseModel() {
    override fun produceFunctionData(): List<FunctionItem>? {
        val functionList = ArrayList<FunctionItem>()
        var function = FunctionItem()
        function.id = 0
        function.name = "列表"
        functionList.add(function)

        function = FunctionItem()
        function.id = 1
        function.name = "通信"
        functionList.add(function)

        function = FunctionItem()
        function.id = 2
        function.name = "View"
        functionList.add(function)

        function = FunctionItem()
        function.id = 3
        function.name = "Page"
        functionList.add(function)

        function = FunctionItem()
        function.id = 4
        function.name = "三方"
        functionList.add(function)
        return functionList
    }

}