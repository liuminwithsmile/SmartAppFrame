package com.io.smartappframe.mvp.presenter

import com.io.base.mvp.BasePresenter
import com.io.smartappframe.mvp.contract.RefreshRecyclerViewContract
import com.io.smartappframe.mvp.model.RefreshRecyclerViewModel

/**
 * Description:带有刷新功能的RecyclerView的Presenter
 * 2021-03-16 9:23
 * @author LiuMin
 */
class RefreshRecyclerViewPresenter(view: RefreshRecyclerViewContract.IMainView) :
    BasePresenter<RefreshRecyclerViewContract.IMainModel, RefreshRecyclerViewContract.IMainView>() {
    private var view: RefreshRecyclerViewContract.IMainView
    private var model: RefreshRecyclerViewContract.IMainModel

    init {
        this.view = view
        model = RefreshRecyclerViewModel()
    }

    fun getList(startIndex: Int, length:Int) {
        val list = model.produceData(startIndex, length)
        view.showSuccess(list)
    }
}