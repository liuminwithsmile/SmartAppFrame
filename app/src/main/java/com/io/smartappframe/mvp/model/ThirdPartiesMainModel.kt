package com.io.smartappframe.mvp.model

import com.io.base.mvp.BaseModel
import com.io.smartappframe.mvp.contract.ThirdPartiesMainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:
 * 2021-04-16 10:17
 * @author LiuMin
 */
class ThirdPartiesMainModel: ThirdPartiesMainContract.IMainModel, BaseModel(){
    override fun produceData(): MutableList<FunctionItem>? {
        val list = mutableListOf<FunctionItem>()
        var item = FunctionItem()
        item.id = 0
        item.name = "高德地图-定位"
        list.add(item)

        item = FunctionItem()
        item.id = 1
        item.name = "ZXing-二维码"
        list.add(item)
        return list
    }
}