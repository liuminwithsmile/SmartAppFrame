package com.io.smartappframe.mvp.presenter

import android.content.Context
import android.content.Intent
import com.io.base.mvp.BasePresenter
import com.io.smartappframe.mvc.activity.ImageGroupViewActivity
import com.io.smartappframe.mvp.contract.MainContract
import com.io.smartappframe.mvp.model.MainModel
import com.io.smartappframe.mvp.ui.activity.*

/**
 * Description:展示功能列表的Presenter
 * 2021-03-03 17:57
 * @author LiuMin
 */
class MainPresenter(mainView: MainContract.IMainView):
    BasePresenter<MainContract.IMainModel, MainContract.IMainView>() {

    private var mainView: MainContract.IMainView
    private var mainModel:MainContract.IMainModel

    init {
        this.mainView = mainView
        mainModel = MainModel()
    }

    /**
     * 获取数据列表并显示
     */
    fun getFunctionList() {
        val functionList = mainModel.produceFunctionData()
        functionList?.let {
            if(it.size > 0){
                mainView.showSuccess(it)
            } else {
                mainView.showFail("数据为空")
            }
        }?:let {
            mainView.showFail("数据为空")
        }
    }

    /**
     * 根据function的id决定进入哪个页面
     */
    fun goOtherPage(id:Int, context: Context){
        when(id){
            0->{
                context.startActivity(Intent(context, ListViewMainActivity::class.java))
            }
            1->{
                context.startActivity(Intent(context, CommonLinkMainActivity::class.java))
            }
            2->{
                context.startActivity(Intent(context, ViewMainActivity::class.java))
            }
            3->{

            }
            4->{
                context.startActivity(Intent(context, ThirdPartiesMainActivity::class.java))
            }
        }
    }
}