package com.io.smartappframe.mvp.contract

import com.io.base.mvp.IModel
import com.io.base.mvp.IView
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:通信工具测试类主页Contract
 * 2021-03-24 10:45
 * @author LiuMin
 */
interface CommonLinkMainContract {
    interface IMainModel : IModel {
        fun produceData(): MutableList<FunctionItem>?
    }

    interface IMainView : IView {
        fun showSuccess(list: MutableList<FunctionItem>?)
        fun showFail(msg: String)
    }
}