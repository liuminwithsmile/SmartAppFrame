package com.io.smartappframe.mvp.presenter

import com.io.base.mvp.BasePresenter
import com.io.smartappframe.mvp.contract.StaggeredGridRecyclerViewContract
import com.io.smartappframe.mvp.model.StaggeredGridRecyclerViewModel

/**
 * Description:瀑布流布局列表的Presenter
 * 2021-03-19 17:59
 * @author LiuMin
 */
class StaggeredGridRecyclerViewPresenter(view: StaggeredGridRecyclerViewContract.IMainView) :
    BasePresenter<StaggeredGridRecyclerViewContract.IMainModel,
            StaggeredGridRecyclerViewContract.IMainView>() {
    private var view:StaggeredGridRecyclerViewContract.IMainView
    private var model:StaggeredGridRecyclerViewContract.IMainModel

    init {
        this.view = view
        model = StaggeredGridRecyclerViewModel()
    }

    fun getList() {
        val list = model.produceData()
        view.showSuccess(list)
    }
}