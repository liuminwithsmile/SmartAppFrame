package com.io.smartappframe.mvp.contract

import com.io.base.mvp.IModel
import com.io.base.mvp.IView
import com.io.smartappframe.mvp.model.entity.Friend

/**
 * Description:瀑布流布局列表的Contract
 * 2021-03-19 17:48
 * @author LiuMin
 */
interface StaggeredGridRecyclerViewContract {
    interface IMainModel : IModel {
        fun produceData(): MutableList<Friend>?
    }

    interface IMainView : IView {
        fun showSuccess(list: MutableList<Friend>?)
        fun showFail(msg: String)
    }
}