package com.io.smartappframe.mvp.presenter

import android.content.Context
import android.content.Intent
import com.io.base.mvp.BasePresenter
import com.io.smartappframe.mvc.activity.*
import com.io.smartappframe.mvp.contract.ViewMainContract
import com.io.smartappframe.mvp.model.ViewMainModel

/**
 * Description:展示自定义View的Presenter
 * 2021-04-02 9:21
 * @author LiuMin
 */
class ViewMainPresenter(view: ViewMainContract.IMainView) :
    BasePresenter<ViewMainContract.IMainModel, ViewMainContract.IMainView>() {
    private var model:ViewMainContract.IMainModel
    private var mainView:ViewMainContract.IMainView

    init {
        model = ViewMainModel()
        this.mainView = view
    }

    /**
     * 获取数据列表并显示
     */
    fun getFunctionList() {
        val list = model.produceData()
        list?.let {
            if (it.size > 0) {
                mainView.showSuccess(it)
            } else {
                mainView.showFail("数据为空")
            }
        } ?: let {
            mainView.showFail("数据为空")
        }
    }

    /**
     * 根据function的id决定进入哪个页面
     */
    fun goOtherPage(id: Int, context: Context) {
        when (id) {
            0 -> {
                context.startActivity(
                    Intent(
                        context,
                        ImageGroupViewActivity::class.java
                    )
                )
            }
            1 -> {
                context.startActivity(
                    Intent(
                        context,
                        WaterMaskImageActivity::class.java
                    )
                )
            }
            2 -> {
                context.startActivity(
                    Intent(
                        context,
                        ButtonsActivity::class.java
                    )
                )
            }
            3 -> {
                context.startActivity(
                    Intent(
                        context,
                        EditTextActivity::class.java
                    )
                )
            }
            4 -> {
                context.startActivity(
                    Intent(
                        context,
                        ToastActivity::class.java
                    )
                )
            }
            5 -> {
                context.startActivity(
                    Intent(
                        context,
                        CameraMainActivity::class.java
                    )
                )
            }
        }
    }
}