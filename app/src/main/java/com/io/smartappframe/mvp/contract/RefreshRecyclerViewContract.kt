package com.io.smartappframe.mvp.contract

import com.io.base.mvp.IModel
import com.io.base.mvp.IView
import com.io.smartappframe.mvp.model.entity.Friend

/**
 * Description:带有刷新功能的RecyclerView的Contract
 * 2021-03-15 18:12
 * @author LiuMin
 */
interface RefreshRecyclerViewContract {
    interface IMainModel : IModel {
        fun produceData(startIndex: Int, length:Int): MutableList<Friend>?
    }

    interface IMainView : IView {
        fun showSuccess(list: MutableList<Friend>?)
        fun showFail(msg: String)
    }
}