package com.io.smartappframe.mvp.model

import com.io.base.mvp.BaseModel
import com.io.smartappframe.mvp.contract.GridRecyclerViewContract
import com.io.smartappframe.mvp.model.entity.Friend

/**
 * Description:网格布局列表的Model
 * 2021-03-19 15:27
 * @author LiuMin
 */
class GridRecyclerViewModel:GridRecyclerViewContract.IMainModel, BaseModel() {
    override fun produceData(): MutableList<Friend>? {
        val list = mutableListOf<Friend>()
        for (i in 0..9) {
            var friend = Friend()
            friend.id = i
            friend.name = "无名$i"
            friend.jobTitle = "职称$i"
            list.add(friend)
        }
        return list
    }
}