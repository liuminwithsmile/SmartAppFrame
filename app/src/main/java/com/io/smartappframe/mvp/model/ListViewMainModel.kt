package com.io.smartappframe.mvp.model

import com.io.base.mvp.BaseModel
import com.io.smartappframe.mvp.contract.ListViewMainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:列表主页面的Model
 * 2021-03-15 14:41
 * @author LiuMin
 */
class ListViewMainModel:ListViewMainContract.IListViewMainModel, BaseModel() {
    override fun produceFunctionData(): List<FunctionItem>? {
        val functionList = ArrayList<FunctionItem>()
        var function = FunctionItem()
        function.id = 0
        function.name = "普通RecyclerView+SmartRefresh"
        functionList.add(function)

        function = FunctionItem()
        function.id = 1
        function.name = "网格布局列表"
        functionList.add(function)

        function = FunctionItem()
        function.id = 2
        function.name = "瀑布流布局列表"
        functionList.add(function)

        return functionList
    }
}