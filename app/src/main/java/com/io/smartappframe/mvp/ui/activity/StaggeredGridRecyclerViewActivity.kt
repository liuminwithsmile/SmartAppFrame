package com.io.smartappframe.mvp.ui.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.io.base.mvp.BaseActivity
import com.io.commonlib.helper.ToastHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityGridRecyclerViewBinding
import com.io.smartappframe.mvp.contract.StaggeredGridRecyclerViewContract
import com.io.smartappframe.mvp.model.entity.Friend
import com.io.smartappframe.mvp.presenter.StaggeredGridRecyclerViewPresenter
import com.io.smartappframe.mvp.ui.adapter.StaggeredGridRecyclerViewAdapter

/**
 * Description:瀑布流布局列表
 * 2021-03-20 9:46
 * @author LiuMin
 */
class StaggeredGridRecyclerViewActivity :
    BaseActivity<StaggeredGridRecyclerViewPresenter, ActivityGridRecyclerViewBinding>(),
StaggeredGridRecyclerViewContract.IMainView, View.OnClickListener{

    private lateinit var list: MutableList<Friend>
    private lateinit var adapter: StaggeredGridRecyclerViewAdapter

    override fun initView(savedInstanceState: Bundle?): ActivityGridRecyclerViewBinding {
        return ActivityGridRecyclerViewBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("瀑布流布局列表")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mPresenter = StaggeredGridRecyclerViewPresenter(this)
        initRecyclerView()
        mPresenter?.getList()
    }

    override fun showSuccess(friendList: MutableList<Friend>?) {
        friendList?.let {
            list = friendList
            adapter.notifyData(list)
        }
    }

    override fun showFail(msg: String) {
        ToastHelper.showShortToast(applicationContext, msg)
    }

    override fun onClick(v: View?) {
        v?.let {
            if(it.id == R.id.iv_toolbar_back){
                finish()
            }
        }
    }

    private fun initRecyclerView() {
        list = ArrayList()
        adapter = StaggeredGridRecyclerViewAdapter()
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        mViewBinding.recyclerView.layoutManager = layoutManager
        mViewBinding.recyclerView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        ToastHelper.cancelToast()
    }
}