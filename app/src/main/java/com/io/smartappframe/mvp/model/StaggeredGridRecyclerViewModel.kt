package com.io.smartappframe.mvp.model

import com.io.base.mvp.BaseModel
import com.io.smartappframe.mvp.contract.StaggeredGridRecyclerViewContract
import com.io.smartappframe.mvp.model.entity.Friend

/**
 * Description:瀑布流布局列表的Model
 * 2021-03-19 17:57
 * @author LiuMin
 */
class StaggeredGridRecyclerViewModel: StaggeredGridRecyclerViewContract.IMainModel, BaseModel() {
    override fun produceData(): MutableList<Friend>? {
        val list = mutableListOf<Friend>()
        for (i in 0..9) {
            var friend = Friend()
            friend.id = i
            friend.name = "无名$i"
            friend.jobTitle = "职称$i"
            list.add(friend)
        }
        return list
    }
}