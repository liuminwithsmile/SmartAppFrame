package com.io.smartappframe.mvp.ui.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewbinding.ViewBinding
import com.io.base.mvp.BaseActivity
import com.io.base.mvp.BaseAdapter
import com.io.commonlib.helper.ToastHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityViewMainBinding
import com.io.smartappframe.mvp.contract.ThirdPartiesMainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem
import com.io.smartappframe.mvp.presenter.ThirdPartiesMainPresenter
import com.io.smartappframe.mvp.ui.adapter.MainAdapter

/**
 * Description:第三方控件
 * 2021-04-16 10:54
 * @author LiuMin
 */
class ThirdPartiesMainActivity : BaseActivity<ThirdPartiesMainPresenter, ActivityViewMainBinding>(),
    ThirdPartiesMainContract.IMainView, View.OnClickListener,
    BaseAdapter.OnRecyclerViewItemClickListener<FunctionItem> {

    private var list: List<FunctionItem>? = null
    private var adapter: MainAdapter? = null

    override fun initView(savedInstanceState: Bundle?): ActivityViewMainBinding {
        return ActivityViewMainBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("第三方控件展示")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mPresenter = ThirdPartiesMainPresenter(this)
        initRecyclerView()
        mPresenter?.getFunctionList()
    }

    override fun showSuccess(itemList: MutableList<FunctionItem>?) {
        itemList?.let {
            list = itemList
            adapter?.setOnItemClickListener(this)
            adapter?.notifyData(list)
        }
    }

    override fun showFail(msg: String) {
        ToastHelper.showShortToast(applicationContext, msg)
    }

    override fun onClick(v: View?) {
        v?.let {
            if(it.id == R.id.iv_toolbar_back){
                finish()
            }
        }
    }

    override fun onItemClick(
        view: View,
        viewBinding: ViewBinding?,
        data: FunctionItem,
        position: Int
    ) {
        mPresenter?.goOtherPage(data.id, this)
    }

    private fun initRecyclerView() {
        list = mutableListOf()
        adapter = MainAdapter()
        mViewBinding.recyclerView.adapter = adapter
        val layoutManager = GridLayoutManager(this, 2)
        mViewBinding.recyclerView.layoutManager = layoutManager
    }

    override fun onDestroy() {
        super.onDestroy()
        ToastHelper.cancelToast()
    }
}