package com.io.smartappframe.mvp.contract

import com.io.base.mvp.IModel
import com.io.base.mvp.IView
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:列表主页面的Contract
 * 2021-03-15 14:41
 * @author LiuMin
 */
interface ListViewMainContract {
    interface IListViewMainModel : IModel {
        fun produceFunctionData():List<FunctionItem>?
    }
    interface IListViewMainView: IView {
        fun showSuccess(functionList:List<FunctionItem>)
        fun showFail(msg: String)
    }
}