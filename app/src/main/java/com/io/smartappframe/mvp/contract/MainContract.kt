package com.io.smartappframe.mvp.contract

import com.io.base.mvp.IModel
import com.io.base.mvp.IView
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * 功能列表页的Contract
 */
interface MainContract {

    interface IMainModel: IModel {
        fun produceFunctionData():List<FunctionItem>?
    }

    interface IMainView: IView {
        fun showSuccess(functionList:List<FunctionItem>)
        fun showFail(msg: String)
    }
}