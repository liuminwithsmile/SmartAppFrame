package com.io.smartappframe.mvp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.io.base.mvp.BaseAdapter
import com.io.base.mvp.BaseHolder
import com.io.smartappframe.databinding.ItemFunctionBinding
import com.io.smartappframe.mvp.model.entity.FunctionItem

class MainAdapter : BaseAdapter<FunctionItem>() {
    override fun getViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ViewBinding {
        return ItemFunctionBinding.inflate(inflater, parent, false)
    }

    override fun setData(
        holder: BaseHolder,
        viewBinding: ViewBinding,
        data: FunctionItem,
        position: Int
    ) {
        val binding = viewBinding as ItemFunctionBinding
        holder.addOnItemClickListener(binding.layoutFunction)
        binding.tvFunction.setText("${position + 1}、${data.name}")
    }
}