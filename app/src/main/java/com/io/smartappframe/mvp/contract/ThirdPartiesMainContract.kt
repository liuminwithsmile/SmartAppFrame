package com.io.smartappframe.mvp.contract

import com.io.base.mvp.IModel
import com.io.base.mvp.IView
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:第三方控件列表页面
 * 2021-04-16 10:16
 * @author LiuMin
 */
class ThirdPartiesMainContract {
    interface IMainModel : IModel {
        fun produceData(): MutableList<FunctionItem>?
    }

    interface IMainView : IView {
        fun showSuccess(list: MutableList<FunctionItem>?)
        fun showFail(msg: String)
    }
}