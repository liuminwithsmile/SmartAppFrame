package com.io.smartappframe.mvp.ui.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewbinding.ViewBinding
import com.io.base.mvp.BaseActivity
import com.io.base.mvp.BaseAdapter
import com.io.commonlib.helper.ToastHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityListViewMainBinding
import com.io.smartappframe.mvp.contract.ListViewMainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem
import com.io.smartappframe.mvp.presenter.ListViewMainPresenter
import com.io.smartappframe.mvp.ui.adapter.MainAdapter

class ListViewMainActivity : BaseActivity<ListViewMainPresenter, ActivityListViewMainBinding>(),
    ListViewMainContract.IListViewMainView, BaseAdapter.OnRecyclerViewItemClickListener<FunctionItem>,
    View.OnClickListener {

    private var list: List<FunctionItem>? = null
    private var adapter: MainAdapter? = null

    override fun initView(savedInstanceState: Bundle?): ActivityListViewMainBinding {
        return ActivityListViewMainBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("列表控件展示")
        //mViewBinding.header.ivToolbarBack.visibility = View.VISIBLE
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mPresenter = ListViewMainPresenter(this)
        initRecyclerView()
        mPresenter?.getFunctionList()
    }

    override fun showSuccess(functionList: List<FunctionItem>) {
        list = functionList
        mViewBinding.recyclerView.adapter = adapter
        adapter?.setOnItemClickListener(this)
        adapter?.notifyData(list)
    }

    override fun showFail(msg: String) {
        ToastHelper.showShortToast(applicationContext, msg)
    }

    override fun onItemClick(
        view: View,
        viewBinding: ViewBinding?,
        data: FunctionItem,
        position: Int
    ) {
        mPresenter?.goOtherPage(data.id, this)
    }

    private fun initRecyclerView() {
        list = mutableListOf()
        adapter = MainAdapter()
        val layoutManager = LinearLayoutManager(this)
        mViewBinding.recyclerView.layoutManager = layoutManager
    }

    override fun onClick(v: View?) {
        v?.let {
            if(it.id == R.id.iv_toolbar_back){
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ToastHelper.cancelToast()
    }

}