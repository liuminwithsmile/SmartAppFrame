package com.io.smartappframe.mvp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.io.base.mvp.BaseAdapter
import com.io.base.mvp.BaseHolder
import com.io.smartappframe.databinding.ItemRefreshRecyclerViewBinding
import com.io.smartappframe.mvp.model.entity.Friend

/**
 * Description:带有刷新功能的RecyclerView的Adapter
 * 2021-03-16 9:32
 * @author LiuMin
 */
class RefreshRecyclerViewAdapter: BaseAdapter<Friend>() {
    //第几个项获取了焦点
    var currentFocusIndex = 0
    override fun getViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ViewBinding {
        return ItemRefreshRecyclerViewBinding.inflate(inflater, parent, false)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun setData(
        holder: BaseHolder,
        viewBinding: ViewBinding,
        data: Friend,
        position: Int
    ) {
        val binding = viewBinding as ItemRefreshRecyclerViewBinding
        binding.tvId.setText(data.id.toString())
        binding.tvName.setText(data.name)
        binding.tvJobTitle.setText(data.jobTitle)
        if(data.id % 3 == 0){
            binding.imageHeader.visibility = View.GONE
        }
        binding.editText.setOnFocusChangeListener { v, hasFocus ->
            if(hasFocus){
                currentFocusIndex = position
            } else {
                currentFocusIndex = 0
            }
        }
    }
}