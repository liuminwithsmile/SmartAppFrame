package com.io.smartappframe.mvp.presenter

import android.content.Context
import android.content.Intent
import com.io.base.mvp.BasePresenter
import com.io.smartappframe.mvp.contract.ListViewMainContract
import com.io.smartappframe.mvp.model.ListViewMainModel
import com.io.smartappframe.mvp.ui.activity.GridRecyclerViewActivity
import com.io.smartappframe.mvp.ui.activity.RefreshRecyclerViewActivity
import com.io.smartappframe.mvp.ui.activity.StaggeredGridRecyclerViewActivity

/**
 * Description:列表主页面的Presenter
 * 2021-03-15 14:51
 * @author LiuMin
 */
class ListViewMainPresenter(listViewMainView: ListViewMainContract.IListViewMainView) :
    BasePresenter<ListViewMainContract.IListViewMainModel, ListViewMainContract.IListViewMainView>() {
    private var listViewMainView: ListViewMainContract.IListViewMainView
    private var listViewMainModel: ListViewMainContract.IListViewMainModel

    init {
        this.listViewMainView = listViewMainView
        listViewMainModel = ListViewMainModel()
    }

    /**
     * 获取数据列表并显示
     */
    fun getFunctionList() {
        val functionList = listViewMainModel.produceFunctionData()
        functionList?.let {
            if (it.size > 0) {
                listViewMainView.showSuccess(it)
            } else {
                listViewMainView.showFail("数据为空")
            }
        } ?: let {
            listViewMainView.showFail("数据为空")
        }
    }

    /**
     * 根据function的id决定进入哪个页面
     */
    fun goOtherPage(id: Int, context: Context) {
        when (id) {
            0 -> {
                context.startActivity(Intent(context, RefreshRecyclerViewActivity::class.java))
            }
            1 -> {
                context.startActivity(Intent(context, GridRecyclerViewActivity::class.java))
            }
            2 -> {
                context.startActivity(
                    Intent(
                        context,
                        StaggeredGridRecyclerViewActivity::class.java
                    )
                )
            }

        }
    }
}