package com.io.smartappframe.mvp.model.entity

/**
 * 需要展示的功能的实体类
 */
class FunctionItem {
    var id: Int = 0 //功能id
    var name: String = "" //功能名称

    override fun toString(): String {
        return "FunctionItem(id=$id, name='$name')"
    }
}