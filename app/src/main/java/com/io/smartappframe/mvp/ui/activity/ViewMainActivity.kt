package com.io.smartappframe.mvp.ui.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewbinding.ViewBinding
import com.io.base.mvp.BaseActivity
import com.io.base.mvp.BaseAdapter
import com.io.commonlib.helper.ToastHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityViewMainBinding
import com.io.smartappframe.mvp.contract.ViewMainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem
import com.io.smartappframe.mvp.presenter.ViewMainPresenter
import com.io.smartappframe.mvp.ui.adapter.MainAdapter

/**
 * Description:展示自定义View的主页面
 * 2021-04-02 9:29
 * @author LiuMin
 */
class ViewMainActivity : BaseActivity<ViewMainPresenter, ActivityViewMainBinding>(),
    ViewMainContract.IMainView, View.OnClickListener,
    BaseAdapter.OnRecyclerViewItemClickListener<FunctionItem> {

    private var list: List<FunctionItem>? = null
    private var adapter: MainAdapter? = null

    override fun initView(savedInstanceState: Bundle?): ActivityViewMainBinding {
        return ActivityViewMainBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("自定义View展示")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mPresenter = ViewMainPresenter(this)
        initRecyclerView()
        mPresenter?.getFunctionList()
    }

    override fun showSuccess(itemList: MutableList<FunctionItem>?) {
        itemList?.let {
            list = itemList
            adapter?.setOnItemClickListener(this)
            adapter?.notifyData(list)
        }
    }

    override fun showFail(msg: String) {
        ToastHelper.showShortToast(applicationContext, msg)
    }

    override fun onClick(v: View?) {
        v?.let {
            if(it.id == R.id.iv_toolbar_back){
                finish()
            }
        }
    }

    override fun onItemClick(
        view: View,
        viewBinding: ViewBinding?,
        data: FunctionItem,
        position: Int
    ) {
        mPresenter?.goOtherPage(data.id, this)
    }

    private fun initRecyclerView() {
        list = mutableListOf()
        adapter = MainAdapter()
        mViewBinding.recyclerView.adapter = adapter
        val layoutManager = GridLayoutManager(this, 2)
        mViewBinding.recyclerView.layoutManager = layoutManager
    }

    override fun onDestroy() {
        super.onDestroy()
        ToastHelper.cancelToast()
    }
}