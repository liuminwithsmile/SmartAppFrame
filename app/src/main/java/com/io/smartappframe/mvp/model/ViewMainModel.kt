package com.io.smartappframe.mvp.model

import com.io.base.mvp.BaseModel
import com.io.smartappframe.mvp.contract.ViewMainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:展示自定义View的Model
 * 2021-04-02 9:19
 * @author LiuMin
 */
class ViewMainModel:ViewMainContract.IMainModel, BaseModel() {
    override fun produceData(): MutableList<FunctionItem>? {
        val list = mutableListOf<FunctionItem>()
        var item = FunctionItem()
        item.id = 0
        item.name = "ImageGroupView"
        list.add(item)

        item = FunctionItem()
        item.id = 1
        item.name = "水印图片"
        list.add(item)

        item = FunctionItem()
        item.id = 2
        item.name = "按钮"
        list.add(item)

        item = FunctionItem()
        item.id = 3
        item.name = "输入框"
        list.add(item)

        item = FunctionItem()
        item.id = 4
        item.name = "自定义Toast"
        list.add(item)

        item = FunctionItem()
        item.id = 5
        item.name = "相机预览"
        list.add(item)

        return list
    }
}