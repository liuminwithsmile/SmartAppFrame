package com.io.smartappframe.mvp.presenter

import android.content.Context
import android.content.Intent
import com.io.base.mvp.BasePresenter
import com.io.smartappframe.mvc.activity.LANActivity
import com.io.smartappframe.mvc.activity.MQTTActivity
import com.io.smartappframe.mvc.activity.SerialPortActivity
import com.io.smartappframe.mvp.contract.CommonLinkMainContract
import com.io.smartappframe.mvp.model.CommonLinkMainModel
import com.io.smartappframe.mvp.ui.activity.GridRecyclerViewActivity
import com.io.smartappframe.mvp.ui.activity.RefreshRecyclerViewActivity
import com.io.smartappframe.mvp.ui.activity.StaggeredGridRecyclerViewActivity

/**
 * Description:通信工具测试类主页Presenter
 * 2021-03-24 11:03
 * @author LiuMin
 */
class CommonLinkMainPresenter(view: CommonLinkMainContract.IMainView) :
    BasePresenter<CommonLinkMainContract.IMainModel, CommonLinkMainContract.IMainView>() {

    private var model: CommonLinkMainContract.IMainModel = CommonLinkMainModel()
    private var mainView: CommonLinkMainContract.IMainView = view

    /**
     * 获取数据列表并显示
     */
    fun getFunctionList() {
        val list = model.produceData()
        list?.let {
            if (it.size > 0) {
                mainView.showSuccess(it)
            } else {
                mainView.showFail("数据为空")
            }
        } ?: let {
            mainView.showFail("数据为空")
        }
    }

    /**
     * 根据function的id决定进入哪个页面
     */
    fun goOtherPage(id: Int, context: Context) {
        when (id) {
            0 -> {

            }
            1 -> {
                context.startActivity(
                    Intent(
                        context,
                        MQTTActivity::class.java
                    )
                )
            }
            2 -> {
                context.startActivity(
                    Intent(
                        context,
                        SerialPortActivity::class.java
                    )
                )
            }
            3 -> {
                context.startActivity(
                    Intent(
                        context,
                        LANActivity::class.java
                    )
                )
            }
        }
    }
}