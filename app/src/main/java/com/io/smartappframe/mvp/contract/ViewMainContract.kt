package com.io.smartappframe.mvp.contract

import com.io.base.mvp.IModel
import com.io.base.mvp.IView
import com.io.smartappframe.mvp.model.entity.FunctionItem

/**
 * Description:展示自定义View的主页
 * 2021-04-02 9:14
 * @author LiuMin
 */
interface ViewMainContract {
    interface IMainModel : IModel {
        fun produceData(): MutableList<FunctionItem>?
    }

    interface IMainView : IView {
        fun showSuccess(list: MutableList<FunctionItem>?)
        fun showFail(msg: String)
    }
}