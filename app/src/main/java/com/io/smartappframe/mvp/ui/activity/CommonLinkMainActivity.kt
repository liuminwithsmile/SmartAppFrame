package com.io.smartappframe.mvp.ui.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewbinding.ViewBinding
import com.io.base.mvp.BaseActivity
import com.io.base.mvp.BaseAdapter
import com.io.commonlib.helper.ToastHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityCommonLinkMainBinding
import com.io.smartappframe.mvp.contract.CommonLinkMainContract
import com.io.smartappframe.mvp.model.entity.FunctionItem
import com.io.smartappframe.mvp.presenter.CommonLinkMainPresenter
import com.io.smartappframe.mvp.ui.adapter.MainAdapter

class CommonLinkMainActivity : BaseActivity<CommonLinkMainPresenter, ActivityCommonLinkMainBinding>(),
CommonLinkMainContract.IMainView, View.OnClickListener, BaseAdapter.OnRecyclerViewItemClickListener<FunctionItem>{

    private var list: List<FunctionItem>? = null
    private var adapter: MainAdapter? = null

    override fun initView(savedInstanceState: Bundle?): ActivityCommonLinkMainBinding {
        return ActivityCommonLinkMainBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("通信工具类展示")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mPresenter = CommonLinkMainPresenter(this)
        initRecyclerView()
        mPresenter?.getFunctionList()
    }

    override fun showSuccess(itemList: MutableList<FunctionItem>?) {
        itemList?.let {
            list = itemList
            adapter?.setOnItemClickListener(this)
            adapter?.notifyData(list)
        }
    }

    override fun showFail(msg: String) {
        ToastHelper.showShortToast(applicationContext, msg)
    }

    override fun onClick(v: View?) {
        v?.let {
            if(it.id == R.id.iv_toolbar_back){
                finish()
            }
        }
    }

    private fun initRecyclerView() {
        list = mutableListOf()
        adapter = MainAdapter()
        mViewBinding.recyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(this)
        mViewBinding.recyclerView.layoutManager = layoutManager
    }

    override fun onItemClick(
        view: View,
        viewBinding: ViewBinding?,
        data: FunctionItem,
        position: Int
    ) {
        mPresenter?.goOtherPage(data.id, this)
    }

    override fun onDestroy() {
        super.onDestroy()
        ToastHelper.cancelToast()
    }
}