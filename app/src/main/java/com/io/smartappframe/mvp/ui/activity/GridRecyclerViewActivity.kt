package com.io.smartappframe.mvp.ui.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.io.base.mvp.BaseActivity
import com.io.commonlib.helper.ToastHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityGridRecyclerViewBinding
import com.io.smartappframe.mvp.contract.GridRecyclerViewContract
import com.io.smartappframe.mvp.model.entity.Friend
import com.io.smartappframe.mvp.presenter.GridRecyclerViewPresenter
import com.io.smartappframe.mvp.ui.adapter.GridRecyclerViewAdapter

/**
 * Description:网格布局列表页面
 * 2021-03-19 16:57
 * @author LiuMin
 */
class GridRecyclerViewActivity :
    BaseActivity<GridRecyclerViewPresenter, ActivityGridRecyclerViewBinding>(),
    GridRecyclerViewContract.IMainView, View.OnClickListener {
    private lateinit var list: MutableList<Friend>
    private lateinit var adapter: GridRecyclerViewAdapter

    override fun initView(savedInstanceState: Bundle?): ActivityGridRecyclerViewBinding {
        return ActivityGridRecyclerViewBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("网格布局列表")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mPresenter = GridRecyclerViewPresenter(this)
        initRecyclerView()
        mPresenter?.getList()
    }

    override fun showSuccess(friendList: MutableList<Friend>?) {
        friendList?.let {
            list = friendList
            adapter.notifyData(list)
        }
    }

    override fun showFail(msg: String) {
        ToastHelper.showShortToast(applicationContext, msg)
    }

    override fun onClick(v: View?) {
        v?.let {
            if(it.id == R.id.iv_toolbar_back){
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ToastHelper.cancelToast()
    }

    private fun initRecyclerView() {
        list = ArrayList()
        adapter = GridRecyclerViewAdapter()
        val layoutManager = GridLayoutManager(this, 5)
        mViewBinding.recyclerView.layoutManager = layoutManager
        mViewBinding.recyclerView.adapter = adapter
    }
}