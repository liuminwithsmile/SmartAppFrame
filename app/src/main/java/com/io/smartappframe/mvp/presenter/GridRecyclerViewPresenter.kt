package com.io.smartappframe.mvp.presenter

import com.io.base.mvp.BasePresenter
import com.io.smartappframe.mvp.contract.GridRecyclerViewContract
import com.io.smartappframe.mvp.model.GridRecyclerViewModel

/**
 * Description:网格布局列表的Presenter
 * 2021-03-19 15:29
 * @author LiuMin
 */
class GridRecyclerViewPresenter(view: GridRecyclerViewContract.IMainView) :
    BasePresenter<GridRecyclerViewContract.IMainModel, GridRecyclerViewContract.IMainView>() {
    private var view: GridRecyclerViewContract.IMainView
    private var model: GridRecyclerViewContract.IMainModel
    init {
        this.view = view
        model = GridRecyclerViewModel()
    }

    fun getList() {
        val list = model.produceData()
        view.showSuccess(list)
    }
}