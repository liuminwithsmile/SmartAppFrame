package com.io.smartappframe.mvp.model.entity

/**
 * Description:朋友实体类
 * 2021-03-15 18:15
 * @author LiuMin
 */
class Friend {
    var id: Int = 0 //朋友id
    var name: String = "" //朋友姓名
    var jobTitle: String = "" //职称

    override fun toString(): String {
        return "Friend(id=$id, name='$name', jobTitle='$jobTitle')"
    }
}