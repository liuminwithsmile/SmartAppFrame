package com.io.smartappframe.mvc.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.io.base.mvc.BaseActivityMVC
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityCameraMainBinding

/**
 * 相机预览主页面，Camera 和 Camera2实现相机预览的主页面
 */
class CameraMainActivity : BaseActivityMVC<ActivityCameraMainBinding>(), View.OnClickListener {

    override fun initView(savedInstanceState: Bundle?): ActivityCameraMainBinding {
        return ActivityCameraMainBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("相机预览主页面")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mViewBinding.btnCamera.setOnClickListener(this)
        mViewBinding.btnCamera2.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.let {
            if (it.id == R.id.iv_toolbar_back) {
                finish()
            } else if (it.id == R.id.btnCamera) {
                startActivity(Intent(this, CameraActivity::class.java))
            } else if (it.id == R.id.btnCamera2) {
                startActivity(Intent(this, Camera2Activity::class.java))
            }
        }
    }

}