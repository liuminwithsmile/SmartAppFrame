package com.io.smartappframe.mvc.activity

import android.os.Bundle
import android.view.View
import com.io.base.mvc.BaseActivityMVC
import com.io.commonlib.helper.DataHelper
import com.io.commonlib.helper.LogHelper
import com.io.commonlib.helper.ToastHelper
import com.io.commonlink.serialport.SerialPortHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivitySerialPortBinding

/**
 * Description:串口测试类
 * 2021-03-24 11:24
 * @author LiuMin
 */
class SerialPortActivity : BaseActivityMVC<ActivitySerialPortBinding>(), View.OnClickListener,
    SerialPortHelper.ReceivedDataSerialPortListener {
    private lateinit var serialPortHelperLeft: SerialPortHelper
    private lateinit var serialPortHelperRight: SerialPortHelper

    override fun initView(savedInstanceState: Bundle?): ActivitySerialPortBinding {
        return ActivitySerialPortBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("串口测试")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mViewBinding.btnCloseLeft.setOnClickListener(this)
        mViewBinding.btnCloseRight.setOnClickListener(this)
        mViewBinding.btnOpenLeft.setOnClickListener(this)
        mViewBinding.btnOpenRight.setOnClickListener(this)
        mViewBinding.btnSendLeft.setOnClickListener(this)
        mViewBinding.btnSendRight.setOnClickListener(this)
        serialPortHelperLeft = SerialPortHelper()
        serialPortHelperRight = SerialPortHelper()
        LogHelper.d(TAG, "serialPortHelperLeft--" + serialPortHelperLeft)
        LogHelper.d(TAG, "serialPortHelperRight--" + serialPortHelperRight)
    }

    override fun onClick(v: View?) {
        v?.let {
            if (it.id == R.id.iv_toolbar_back) {
                finish()
            } else if (it.id == R.id.btn_open_left) {//打开左边的串口
                openSerialPort(true)
            } else if (it.id == R.id.btn_close_left) {//关闭左边的串口
                closeSerialPort(true)
            } else if (it.id == R.id.btn_send_left) {//发送数据-左边
                sendData(true)
            } else if (it.id == R.id.btn_open_right) {//打开右边的串口
                openSerialPort(false)
            } else if (it.id == R.id.btn_close_right) {//关闭右边的串口
                closeSerialPort(false)
            } else if (it.id == R.id.btn_send_right) {//发送数据-右边
                sendData(false)
            }
        }
    }

    /**
     * 打开串口
     */
    private fun openSerialPort(isLeft: Boolean) {
        if (isLeft) {
            val port = mViewBinding.etPortNumLeft.text.toString()
            val bandRata = mViewBinding.etBandrateLeft.text.toString().toIntOrNull()
            if (!port.isNullOrEmpty() && bandRata != null) {
                serialPortHelperLeft.initSerialPort("/dev/ttyS${port}", bandRata, this)
                serialPortHelperLeft.openSerialPort()
            } else {
                ToastHelper.showShortToast(applicationContext, "端口和波特率不能为空")
            }
        } else {
            val port = mViewBinding.etPortNumRight.text.toString()
            val bandRata = mViewBinding.etBandrateRight.text.toString().toIntOrNull()
            if (!port.isNullOrEmpty() && bandRata != null) {
                serialPortHelperRight.initSerialPort("/dev/ttyS${port}", bandRata, this)
                serialPortHelperRight.openSerialPort()
            } else {
                ToastHelper.showShortToast(applicationContext, "端口和波特率不能为空")
            }
        }
    }

    /**
     * 关闭串口
     */
    private fun closeSerialPort(isLeft: Boolean){
        if(isLeft){
            serialPortHelperLeft.closeSerialPort()
        } else {
            serialPortHelperRight.closeSerialPort()
        }
    }

    /**
     * 发送数据
     */
    private fun sendData(isLeft: Boolean){
        if(isLeft){
            val content = mViewBinding.etSendLeft.text.toString()
            LogHelper.d(TAG, "输入框中的数据:-${content}-")
            if(!content.isNullOrEmpty()){
                serialPortHelperLeft.sendData(DataHelper().hexToByteArray(content))
            } else {
                ToastHelper.showShortToast(applicationContext, "没有需要发送的数据")
            }
        } else {
            val content = mViewBinding.etSendRight.text.toString()
            if(!content.isNullOrEmpty()){
                serialPortHelperRight.sendData(DataHelper().hexToByteArray(content))
            } else {
                ToastHelper.showShortToast(applicationContext, "没有需要发送的数据")
            }
        }
    }

    override fun onReceivedDataSerialPortListener(data: ByteArray?, port: String) {
        LogHelper.d(TAG, "onReceivedDataSerialPortListener--${port}")
    }

    override fun onDestroy() {
        super.onDestroy()
        closeSerialPort(true)
        closeSerialPort(false)
        ToastHelper.cancelToast()
    }
}