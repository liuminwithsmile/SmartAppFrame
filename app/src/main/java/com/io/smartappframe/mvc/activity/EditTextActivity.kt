package com.io.smartappframe.mvc.activity

import android.os.Bundle
import android.view.View
import com.io.base.mvc.BaseActivityMVC
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityEditTextBinding

/**
 * Description: 展示自定义的输入框
 * 2021-05-10 14:57
 * @author LiuMin
 */
class EditTextActivity : BaseActivityMVC<ActivityEditTextBinding>(), View.OnClickListener {
    override fun initView(savedInstanceState: Bundle?): ActivityEditTextBinding {
        return ActivityEditTextBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("自定义输入框展示")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.let {
            if (it.id == R.id.iv_toolbar_back) {
                finish()
            }
        }
    }
}