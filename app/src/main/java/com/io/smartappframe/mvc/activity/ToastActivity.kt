package com.io.smartappframe.mvc.activity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import com.io.base.mvc.BaseActivityMVC
import com.io.commonlib.helper.ToastHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityToastBinding

/**
 * Description:自定义Toast页面
 * 2021-05-21 10:43
 * @author LiuMin
 */
class ToastActivity : BaseActivityMVC<ActivityToastBinding>(), View.OnClickListener {
    override fun initView(savedInstanceState: Bundle?): ActivityToastBinding {
        return ActivityToastBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("自定义Toast")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mViewBinding.btnShowToast.setOnClickListener(this)
        mViewBinding.btnShowLoading.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.let {
            if (it.id == R.id.iv_toolbar_back) {
                finish()
            } else if (it.id == R.id.btn_show_toast) {
                ToastHelper.showLongToast(applicationContext, "测试")
//                val toastView = LayoutInflater.from(this).inflate(R.layout.toast, null)
//                val toast = Toast(this)
//                toast.duration = Toast.LENGTH_SHORT
//                toast.setGravity(Gravity.CENTER, 0, 0)
//                toast.view = toastView
//                toast.show()
            } else if (it.id == R.id.btn_show_loading) {
                showPageLoading()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ToastHelper.cancelToast()
    }

    private fun showPageLoading(){
        progressDialog.show()
        val looper = Looper.myLooper()
        looper?.let {
            val handler = Handler(it)
            handler.postDelayed(object:Runnable{
                override fun run() {
                    progressDialog.dismiss()
                }
            }, 2000)
        }
    }
}