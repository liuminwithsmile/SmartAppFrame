package com.io.smartappframe.mvc.activity

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import com.io.base.mvc.BaseActivityMVC
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityMQTTBinding
import com.io.smartappframe.service.MyMqttService

/**
 * Description:测试MQTT
 * 2021-06-19 18:27
 * @author LiuMin
 */
class MQTTActivity : BaseActivityMVC<ActivityMQTTBinding>(), View.OnClickListener,
    ServiceConnection {

    override fun initView(savedInstanceState: Bundle?): ActivityMQTTBinding {
        return ActivityMQTTBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("MQTT测试")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mViewBinding.btnStart.setOnClickListener(this)
        mViewBinding.btnStop.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.let {
            if (it.id == R.id.iv_toolbar_back) {
                finish()
            } else if (it.id == R.id.btnStart) {
                startMQTT()
            } else if (it.id == R.id.btnStop) {
                stopMQTT()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopMQTT()
    }

    /**
     * 开启MQTT
     */
    private fun startMQTT(){
        val intentMqttService = Intent(this, MyMqttService::class.java)
        bindService(intentMqttService, this, Context.BIND_AUTO_CREATE)
    }

    /**
     * 关闭MQTT
     */
    private fun stopMQTT(){
        unbindService(this)
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {

    }

    override fun onServiceDisconnected(name: ComponentName?) {

    }
}