package com.io.smartappframe.mvc.activity

import android.os.Bundle
import android.view.View
import com.io.base.mvc.BaseActivityMVC
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityButtonsBinding

class ButtonsActivity : BaseActivityMVC<ActivityButtonsBinding>(), View.OnClickListener {

    override fun initView(savedInstanceState: Bundle?): ActivityButtonsBinding {
        return ActivityButtonsBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("按钮展示")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.let {
            if (it.id == R.id.iv_toolbar_back) {
                finish()
            }
        }
    }

}