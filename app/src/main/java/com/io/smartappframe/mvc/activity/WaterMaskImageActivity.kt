package com.io.smartappframe.mvc.activity

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import com.io.base.mvc.BaseActivityMVC
import com.io.commonlib.img.WaterMaskImageHelper
import com.io.smartappframe.R
import com.io.smartappframe.databinding.ActivityWaterMaskImageBinding

/**
 * Description:水印照片的演示demo
 * 2021-04-13 14:28
 * @author LiuMin
 */
class WaterMaskImageActivity : BaseActivityMVC<ActivityWaterMaskImageBinding>(),
    View.OnClickListener {
    private val text = "HelloWorld"
    private val textSize = 50f
    private val padding = 20f
    override fun initView(savedInstanceState: Bundle?): ActivityWaterMaskImageBinding {
        return ActivityWaterMaskImageBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.header.tvTitle.setText("测试ImageGroupView")
        mViewBinding.header.ivToolbarBack.setOnClickListener(this)
        mViewBinding.image.setImageResource(R.drawable.bluesky)
        mViewBinding.btnOriginalImage.setOnClickListener(this)
        mViewBinding.btnTextWaterMaskLeftTop.setOnClickListener(this)
        mViewBinding.btnTextWaterMaskLeftBottom.setOnClickListener(this)
        mViewBinding.btnTextWaterMaskCenter.setOnClickListener(this)
        mViewBinding.btnTextWaterMaskRightTop.setOnClickListener(this)
        mViewBinding.btnTextWaterMaskRightBottom.setOnClickListener(this)
        mViewBinding.btnImageWaterMaskLeftTop.setOnClickListener(this)
        mViewBinding.btnImageWaterMaskLeftBottom.setOnClickListener(this)
        mViewBinding.btnImageWaterMaskCenter.setOnClickListener(this)
        mViewBinding.btnImageWaterMaskRightTop.setOnClickListener(this)
        mViewBinding.btnImageWaterMaskRightBottom.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.let {
            if (it.id == R.id.iv_toolbar_back) {
                finish()
            } else if (it.id == R.id.btn_original_image) {
                mViewBinding.image.setImageResource(R.drawable.bluesky)
            } else if (it.id == R.id.btn_text_water_mask_left_top) {
                creatTextImage(0)
            } else if (it.id == R.id.btn_text_water_mask_left_bottom) {
                creatTextImage(1)
            } else if (it.id == R.id.btn_text_water_mask_center) {
                creatTextImage(2)
            } else if (it.id == R.id.btn_text_water_mask_right_top) {
                creatTextImage(3)
            } else if (it.id == R.id.btn_text_water_mask_right_bottom) {
                creatTextImage(4)
            } else if (it.id == R.id.btn_image_water_mask_left_top) {
                creatImage(0)
            } else if (it.id == R.id.btn_image_water_mask_left_bottom) {
                creatImage(1)
            } else if (it.id == R.id.btn_image_water_mask_center) {
                creatImage(2)
            } else if (it.id == R.id.btn_image_water_mask_right_top) {
                creatImage(3)
            } else if (it.id == R.id.btn_image_water_mask_right_bottom) {
                creatImage(4)
            }
        }
    }

    /**
     * 根据不同[location]值创建文字水印图片
     */
    private fun creatTextImage(location: Int) {
        val localBitmap = BitmapFactory.decodeResource(resources, R.drawable.bluesky)
        var bitmap: Bitmap? = null
        if (location == 0) {
            bitmap = WaterMaskImageHelper().drawTextToLeftTop(
                applicationContext,
                localBitmap,
                text,
                textSize,
                R.color.color_1E2E48, padding, padding
            )
        } else if (location == 1) {
            bitmap = WaterMaskImageHelper().drawTextToLeftBottom(
                applicationContext,
                localBitmap,
                text,
                textSize,
                R.color.color_1E2E48, padding, padding
            )
        } else if (location == 2) {
            bitmap = WaterMaskImageHelper().drawTextToCenter(
                applicationContext,
                localBitmap,
                text,
                textSize,
                R.color.color_1E2E48
            )
        } else if (location == 3) {
            bitmap = WaterMaskImageHelper().drawTextToRightTop(
                applicationContext,
                localBitmap,
                text,
                textSize,
                R.color.color_1E2E48, padding, padding
            )
        } else if (location == 4) {
            bitmap = WaterMaskImageHelper().drawTextToRightBottom(
                applicationContext,
                localBitmap,
                text,
                textSize,
                R.color.color_1E2E48, padding, padding
            )
        }
        mViewBinding.image.setImageBitmap(bitmap)
    }

    /**
     * 根据不同[location]值创建图片水印图片
     */
    private fun creatImage(location: Int) {
        val localBitmap = BitmapFactory.decodeResource(resources, R.drawable.bluesky)
        val qcodeBitmap = BitmapFactory.decodeResource(resources, R.drawable.qcode_liumin)
        var bitmap: Bitmap? = null
        if (location == 0) {
            bitmap = WaterMaskImageHelper().createWaterMaskLeftTop(
                applicationContext,
                localBitmap,
                qcodeBitmap, padding, padding
            )
        } else if (location == 1) {
            bitmap = WaterMaskImageHelper().createWaterMaskLeftBottom(
                applicationContext,
                localBitmap,
                qcodeBitmap, padding, padding
            )
        } else if (location == 2) {
            bitmap = WaterMaskImageHelper().createWaterMaskCenter(
                localBitmap,
                qcodeBitmap
            )
        } else if (location == 3) {
            bitmap = WaterMaskImageHelper().createWaterMaskRightTop(
                applicationContext,
                localBitmap,
                qcodeBitmap, padding, padding
            )
        } else if (location == 4) {
            bitmap = WaterMaskImageHelper().createWaterMaskRightBottom(
                applicationContext,
                localBitmap,
                qcodeBitmap, padding, padding
            )
        }
        mViewBinding.image.setImageBitmap(bitmap)
    }
}