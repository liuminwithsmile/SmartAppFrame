集成极光（包括第三方通道）步骤：
1，在project根目录的build.gradle中配置 mavenCentral 支持（新建 project 默认配置就支持），配置华为和 FCM Maven 代码库
2，在 module 的 gradle 中添加依赖和 AndroidManifest 的替换变量，集成极光推送SDK和厂商通道SDK
3，如果选择的厂商通道包含了HUAWEI厂商通道，则需要在 Huawei 上创建和 JPush 上同包名的待发布应用,
    创建完成后下载该应用的 agconnect-services.json 配置文件并添加到应用的 module 目录下。
4，创建MyJCommonService继承JCommonService，类中什么都不用做，只需要在manifest中配置即可。
5，创建MyJPushMessageReceiver继承自JPushMessageReceiver。
