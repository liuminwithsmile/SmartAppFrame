package com.io.commoncontrol

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.text.Html
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
 * Description:使用文字图标的TextView
 * 2020-09-14 11:02
 * @author LiuMin
 */
class IconTextView : AppCompatTextView {
    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    fun setIconText(iconText: String?) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            this.text = Html.fromHtml(iconText, Html.FROM_HTML_MODE_COMPACT)
        } else {
            this.text = Html.fromHtml(iconText)
        }
    }

    private fun init(context: Context) {
        //设置字体图标
        this.typeface = Typeface.createFromAsset(context.assets, "font/iconfont.ttf")
    }
}