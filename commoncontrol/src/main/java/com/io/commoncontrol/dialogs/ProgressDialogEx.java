package com.io.commoncontrol.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.io.commoncontrol.R;

public class ProgressDialogEx extends Dialog {
    TextView tvLoad;
    public ProgressDialogEx(@NonNull Context context) {
        super(context, R.style.public_dialog_progress);
        setContentView(R.layout.public_dialog_porgress);
        tvLoad = findViewById(R.id.txt_load);
        setCanceledOnTouchOutside(false);
    }

    public void setLoadingContent(int resId){
        tvLoad.setText(resId);
    }

}
