package com.io.commoncontrol

import android.content.Context
import android.util.AttributeSet
import com.youth.banner.Banner
import com.youth.banner.adapter.BannerAdapter

/**
 * 轮播图控件
 */
class Banner<T, BA : BannerAdapter<*, *>?>: Banner<T, BA> {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )
}