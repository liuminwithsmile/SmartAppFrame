package com.io.viewgroup.image.adapter

import android.content.Context
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.io.commoncontrol.image.ZoomImageView
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder

/**
 * Description:Banner控件使用的Adapter
 * 2021-03-26 9:46
 * @author LiuMin
 */
class MyBannerImageAdapter(mList: List<String>?, context: Context) :
    BannerImageAdapter<String>(mList) {
    private var mContext: Context

    init {
        mContext = context
    }

    override fun onBindView(holder: BannerImageHolder?, data: String?, position: Int, size: Int) {
        holder?.let {
            holder.imageView.scaleType = ImageView.ScaleType.FIT_CENTER
            Glide.with(mContext).load(data).into(holder.imageView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerImageHolder {
        val imageView = ZoomImageView(parent.context)
        var params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        imageView.layoutParams = params
        imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        return BannerImageHolder(imageView)
    }
}