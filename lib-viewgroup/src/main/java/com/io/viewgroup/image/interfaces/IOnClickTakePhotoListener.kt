package com.io.viewgroup.image.interfaces

import com.io.viewgroup.image.ImageGroupView

/**
 * Description:点击拍照按钮的监听
 * 2021-03-29 14:18
 * @author LiuMin
 */
interface IOnClickTakePhotoListener {
    fun onClickTakePhoto(imageGroupView: ImageGroupView) //点击拍照菜单
}