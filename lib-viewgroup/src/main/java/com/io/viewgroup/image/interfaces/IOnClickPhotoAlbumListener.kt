package com.io.viewgroup.image.interfaces

import com.io.viewgroup.image.ImageGroupView

/**
 * Description:点击从相册按钮的监听
 * 2021-03-29 14:18
 * @author LiuMin
 */
interface IOnClickPhotoAlbumListener {
    fun onClickPhotoAlbum(imageGroupView: ImageGroupView)
}
