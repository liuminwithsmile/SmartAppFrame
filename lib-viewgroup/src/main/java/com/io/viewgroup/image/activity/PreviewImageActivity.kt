package com.io.viewgroup.image.activity

import android.os.Bundle
import android.view.View
import com.io.base.mvc.BaseActivityMVC
import com.io.commonlib.helper.LogHelper
import com.io.viewgroup.R
import com.io.viewgroup.databinding.ActivityPreviewImageBinding
import com.io.viewgroup.image.adapter.MyBannerImageAdapter
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.listener.OnPageChangeListener

class PreviewImageActivity : BaseActivityMVC<ActivityPreviewImageBinding>(),
    View.OnClickListener, OnPageChangeListener {
    private var mImageList: List<String>? = null //图片数据源
    private var currentPosition = 0 //进入该页面时，默认显示的图片的下标
    override fun initView(savedInstanceState: Bundle?): ActivityPreviewImageBinding {
        return ActivityPreviewImageBinding.inflate(layoutInflater)
    }

    override fun initData(savedInstanceState: Bundle?) {
        mViewBinding.btnDeleteBanner.setOnClickListener(this)
        val list = intent.getStringArrayExtra("ImageGroupViewData")
        list?.let {
            mImageList = list.toList()
        }
        val position = intent.getIntExtra("ImageGroupViewCurrentIndex", 0)
        LogHelper.d(TAG, "实际预览${position}")
        currentPosition = position
        initBanner()
    }

    override fun onStop() {
        super.onStop()
        mViewBinding.banner.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewBinding.banner.destroy()
    }

    override fun onClick(v: View?) {
        v?.let {
            if(v.id == R.id.btn_delete_banner){
                finish()
            }
        }
    }

    private fun initBanner(){
        mViewBinding.banner.isAutoLoop(false)
        mViewBinding.banner.addBannerLifecycleObserver(this)
        mViewBinding.banner.setIndicator(CircleIndicator(this))
        mViewBinding.banner.addOnPageChangeListener(this)
        mViewBinding.banner.setAdapter(MyBannerImageAdapter(mImageList, this), false)
        LogHelper.d(TAG, "initBanner--${currentPosition}")
        mViewBinding.banner.setCurrentItem(currentPosition, false)
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        LogHelper.d(TAG, "onPageScrolled--------${position}")
        currentPosition = position
    }

    override fun onPageSelected(position: Int) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }

}