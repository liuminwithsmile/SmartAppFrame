package com.io.commonlib.img;

//import android.app.Activity;
//
//import com.io.iodevface.R;
//import com.luck.picture.lib.PictureSelector;
//import com.luck.picture.lib.config.PictureConfig;
//import com.luck.picture.lib.config.PictureMimeType;
//import com.luck.picture.lib.entity.LocalMedia;
//import com.luck.picture.lib.language.LanguageConfig;
//import com.luck.picture.lib.tools.PictureFileUtils;
//
//import java.util.List;
//
//public class PictureSelectorHelper {
//    /**
//     * 选择模式-单选，多选
//     */
//    private int selectionMode = PictureConfig.MULTIPLE;
//    /**
//     * 是否支持查看原图
//     */
//    private boolean isSupportOriginal = true;
//
//    /**
//     * 支持的语言-默认支持简体中文
//     */
//    private int language = LanguageConfig.CHINESE;
//    /**
//     * 选择要显示的文件类型
//     */
//    private int chooseMode = PictureMimeType.ofAll();
//    /**
//     * 图片选择的最大数量
//     */
//    private int maxSelectNum = 1;
//    /**
//     * 图片选择的最小数量
//     */
//    private int minSelectNum = 0;
//    /**
//     * 是否显示拍照按钮
//     */
//    private boolean isShowTakePhoto = true;
//    /**
//     * 每行显示的个数
//     */
//    private int imageSpanCount = 4;
//    /**
//     * 单选模式下，选完照片是否直接返回
//     */
//    private boolean isSingleDirectReturn = true;
//    /**
//     * 是否可播放音频
//     */
//    private boolean enablePreviewAudio = true;
//    /**
//     * 是否可预览图片
//     */
//    private boolean enablePreviewImage = true;
//    /**
//     * 是否可预览视频
//     */
//    private boolean enablePreviewVideo = true;
//    /**
//     * 图片列表点击 缩放效果 默认true
//     */
//    private boolean isZoomAnim = true;
//    /**
//     * 是否支持裁剪
//     */
//    private boolean enableCrop = true;
//    /**
//     * 是否支持压缩
//     */
//    private boolean enableCompress = true;
//    /**
//     * 图片压缩质量
//     */
//    private int compressQuality = 80;
//    /**
//     * 是否显示gif
//     */
//    private boolean isShowGif = true;
//
//    /**
//     * 裁剪框是否可拖拽
//     */
//    private boolean freeStyleCropEnabled = true;
//    /**
//     * 是否圆形裁剪
//     */
//    private boolean isCircleDimmedLayer = false;
//    /**
//     * 是否显示裁剪矩形边框 圆形裁剪时建议设为false
//     */
//    private boolean isShowCropFrame = true;
//    /**
//     * 是否显示裁剪矩形网格 圆形裁剪时建议设为false
//     */
//    private boolean isShowCropGrid = true;
//    /**
//     * 最小压缩大小，小于这个数则不压缩，单位kb
//     */
//    private int minimumCompressSize = 100;
//    /**
//     * 裁剪比例-x
//     */
//    private int aspectRatioX = 1;
//    /**
//     * 裁剪比例-y
//     */
//    private int aspectRatioY = 1;
//    //endregion
//
//    public PictureSelectorHelper() {
//
//    }
//
//    public PictureSelectorHelper(PictureSelectorOptions options) {
//        this.aspectRatioX = options.getAspectRatioX();
//        this.aspectRatioY = options.getAspectRatioY();
//        this.chooseMode = options.getChooseMode();
//        this.compressQuality = options.getCompressQuality();
//        this.enablePreviewAudio = options.isEnablePreviewAudio();
//        this.enableCrop = options.isEnableCrop();
//        this.enablePreviewImage = options.isEnablePreviewImage();
//        this.enablePreviewVideo = options.isEnablePreviewVideo();
//        this.freeStyleCropEnabled = options.isFreeStyleCropEnabled();
//        this.imageSpanCount = options.getImageSpanCount();
//        this.isCircleDimmedLayer = options.isCircleDimmedLayer();
//        this.isShowCropFrame = options.isShowCropFrame();
//        this.isShowCropGrid = options.isShowCropGrid();
//        this.isShowGif = options.isShowGif();
//        this.isShowTakePhoto = options.isShowTakePhoto();
//        this.isSingleDirectReturn = options.isSingleDirectReturn();
//        this.isSupportOriginal = options.isSupportOriginal();
//        this.isZoomAnim = options.isZoomAnim();
//        this.language = options.getLanguage();
//        this.maxSelectNum = options.getMaxSelectNum();
//        this.minimumCompressSize = options.getMinimumCompressSize();
//        this.minSelectNum = options.getMinSelectNum();
//        this.selectionMode = options.getSelectionMode();
//    }
//
//    /**
//     * 开始拍照
//     */
//    public void takePhoto(Activity context) {
//        PictureSelector.create(context).openCamera(chooseMode)
//                .loadImageEngine(GlideEngine.createGlideEngine())
//                .selectionMode(selectionMode)
//                .previewImage(enablePreviewImage)
//                .previewVideo(enablePreviewVideo)
//                .enablePreviewAudio(enablePreviewAudio)
//                .isCamera(isShowTakePhoto)
//                .enableCrop(enableCrop)
//                .compress(enableCompress)
//                .compressQuality(compressQuality)
//                .withAspectRatio(aspectRatioX, aspectRatioY)
//                .freeStyleCropEnabled(freeStyleCropEnabled)
//                .circleDimmedLayer(isCircleDimmedLayer)
//                .showCropFrame(isShowCropFrame)
//                .showCropGrid(isShowCropGrid)
//                //预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
//                .previewEggs(false)
//                .minimumCompressSize(minimumCompressSize)
//                .forResult(PictureConfig.CHOOSE_REQUEST);
//    }
//
//    /**
//     * 从相册选择
//     */
//    public void selectPicture(Activity context, List<LocalMedia> selectList) {
//        // 进入相册 以下是例子：不需要的api可以不写
//        PictureSelector.create(context)
//                .openGallery(chooseMode)
//                .loadImageEngine(GlideEngine.createGlideEngine())
//                .setLanguage(language)
//                .isWithVideoImage(isShowTakePhoto)
//                .maxSelectNum(maxSelectNum)
//                .minSelectNum(minSelectNum)
//                .imageSpanCount(imageSpanCount)
//                .isOriginalImageControl(isSupportOriginal)
//                .selectionMode(selectionMode)
//                .isSingleDirectReturn(isSingleDirectReturn)
//                .previewImage(enablePreviewImage)
//                .previewVideo(enablePreviewVideo)
//                .enablePreviewAudio(enablePreviewAudio)
//                .isCamera(isShowTakePhoto)
//                .isZoomAnim(isZoomAnim)
//                .enableCrop(enableCrop)
//                .compress(enableCompress)
//                .compressQuality(compressQuality)
//                .withAspectRatio(aspectRatioX, aspectRatioY)
//                .isGif(isShowGif)
//                .freeStyleCropEnabled(freeStyleCropEnabled)
//                .circleDimmedLayer(isCircleDimmedLayer)
//                .showCropFrame(isShowCropFrame)
//                .showCropGrid(isShowCropGrid)
//                .selectionMedia(selectList)
//                .minimumCompressSize(minimumCompressSize)
//                .forResult(PictureConfig.CHOOSE_REQUEST);
//    }
//
//    /**
//     * 清楚缓存
//     *
//     * @param context
//     */
//    public void clearCache(Activity context) {
//        //包括裁剪和压缩后的缓存，要在上传成功后调用，type 指的是图片or视频缓存取决于你设置的ofImage或ofVideo 注意：需要系统sd卡权限
//        PictureFileUtils.deleteCacheDirFile(context, chooseMode);
//        // 清除所有缓存 例如：压缩、裁剪、视频、音频所生成的临时文件
//        PictureFileUtils.deleteAllCacheDirFile(context);
//    }
//
//    /**
//     * 预览图片和视频
//     *
//     * @param context        Activity实例
//     * @param isShowDownload 长按是否显示下载菜单
//     * @param position       从第几张开始预览
//     * @param selectList     预览的图片列表
//     */
//    public void previewPicList(Activity context, boolean isShowDownload, int position, List<LocalMedia> selectList) {
//        PictureSelector.create(context)
//                .themeStyle(R.style.picture_default_style)
//                .isNotPreviewDownload(isShowDownload)
//                .loadImageEngine(GlideEngine.createGlideEngine())
//                .openExternalPreview(position, selectList);
//    }
//
//    /**
//     * 预览视频
//     *
//     * @param context Activity实例
//     * @param url     视频路径
//     */
//    public void previewVideo(Activity context, String url) {
//        PictureSelector.create(context).externalPictureVideo(url);
//    }
//}
