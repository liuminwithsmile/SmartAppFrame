package com.io.commonlib.img

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import com.io.commonlib.helper.DensityHelper
import com.io.commonlib.helper.LogHelper

/**
 * Description:添加水印的图片工具类
 * 2021-04-13 9:59
 * @author LiuMin
 */
class WaterMaskImageHelper {
    private val TAG = WaterMaskImageHelper::class.java.simpleName

    //region 公有方法
    /**
     * 设置水印图片在左上角
     */
    fun createWaterMaskLeftTop(
        context: Context, src: Bitmap, watermark: Bitmap, paddingLeft: Float,
        paddingTop: Float
    ): Bitmap {
        return createWaterMaskBitmap(
            src,
            watermark,
            DensityHelper.dp2px(context, paddingLeft).toFloat(),
            DensityHelper.dp2px(context, paddingTop).toFloat()
        )
    }

    /**
     * 设置水印图片在右下角
     */
    fun createWaterMaskRightBottom(
        context: Context, src: Bitmap, watermark: Bitmap, paddingRight: Float,
        paddingBottom: Float
    ): Bitmap {
        return createWaterMaskBitmap(
            src,
            watermark,
            src.width - watermark.width - DensityHelper.dp2px(context, paddingRight).toFloat(),
            src.height - watermark.height - DensityHelper.dp2px(context, paddingBottom).toFloat()
        )
    }

    /**
     * 设置水印图片在右上角
     */
    fun createWaterMaskRightTop(
        context: Context, src: Bitmap, watermark: Bitmap, paddingRight: Float,
        paddingTop: Float
    ): Bitmap {
        return createWaterMaskBitmap(
            src,
            watermark,
            src.width - watermark.width - DensityHelper.dp2px(context, paddingRight).toFloat(),
            DensityHelper.dp2px(context, paddingTop).toFloat()
        )
    }

    /**
     * 设置水印图片在左下角
     */
    fun createWaterMaskLeftBottom(
        context: Context, src: Bitmap, watermark: Bitmap, paddingLeft: Float,
        paddingBottom: Float
    ): Bitmap {
        return createWaterMaskBitmap(
            src,
            watermark,
            DensityHelper.dp2px(context, paddingLeft).toFloat(),
            src.height - watermark.height - DensityHelper.dp2px(context, paddingBottom).toFloat()
        )
    }

    /**
     * 设置水印图片到中间
     */
    fun createWaterMaskCenter(src: Bitmap, watermark: Bitmap): Bitmap {
        return createWaterMaskBitmap(
            src,
            watermark,
            (src.width - watermark.width) / 2f,
            (src.height - watermark.height) / 2f
        )
    }

    /**
     * 添加文字水印到图片左上角
     */
    fun drawTextToLeftTop(
        context: Context, bitmap: Bitmap, text: String, size: Float, color: Int, paddingLeft: Float,
        paddingTop: Float
    ): Bitmap {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = color
        paint.textSize = DensityHelper.dp2px(context, size).toFloat()
        val bounds = Rect()
        paint.getTextBounds(text, 0, text.length, bounds)
        return drawTextToBitmap(
            bitmap,
            text,
            paint,
            DensityHelper.dp2px(context, paddingLeft).toFloat(),
            DensityHelper.dp2px(context, paddingTop).toFloat() + bounds.height()
        )
    }

    /**
     * 添加文字水印到图片右下角
     */
    fun drawTextToRightBottom(
        context: Context, bitmap: Bitmap, text: String, size: Float, color: Int, paddingRight: Float,
        paddingBottom: Float
    ): Bitmap {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = color
        paint.textSize = DensityHelper.dp2px(context, size).toFloat()
        val bounds = Rect()
        paint.getTextBounds(text, 0, text.length, bounds)
        return drawTextToBitmap(
            bitmap,
            text,
            paint,
            bitmap.width - bounds.width() - DensityHelper.dp2px(context, paddingRight).toFloat(),
            bitmap.height - DensityHelper.dp2px(context, paddingBottom).toFloat()
        )
    }

    /**
     * 添加文字水印到图片右上角
     */
    fun drawTextToRightTop(
        context: Context, bitmap: Bitmap, text: String, size: Float, color: Int, paddingRight: Float,
        paddingTop: Float
    ): Bitmap {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = color
        paint.textSize = DensityHelper.dp2px(context, size).toFloat()
        val bounds = Rect()
        paint.getTextBounds(text, 0, text.length, bounds)
        return drawTextToBitmap(
            bitmap,
            text,
            paint,
            bitmap.width - bounds.width() - DensityHelper.dp2px(context, paddingRight).toFloat(),
            DensityHelper.dp2px(context, paddingTop).toFloat() + bounds.height()
        )
    }

    /**
     * 添加文字水印到图片右上角
     */
    fun drawTextToLeftBottom(
        context: Context, bitmap: Bitmap, text: String, size: Float, color: Int, paddingLeft: Float,
        paddingBottom: Float
    ): Bitmap {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = color
        paint.textSize = DensityHelper.dp2px(context, size).toFloat()
        val bounds = Rect()
        paint.getTextBounds(text, 0, text.length, bounds)
        return drawTextToBitmap(
            bitmap,
            text,
            paint,
            DensityHelper.dp2px(context, paddingLeft).toFloat(),
            bitmap.height - DensityHelper.dp2px(context, paddingBottom).toFloat()
        )
    }

    /**
     * 添加文字水印到图片右上角
     */
    fun drawTextToCenter(
        context: Context, bitmap: Bitmap, text: String, size: Float, color: Int): Bitmap {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = color
        paint.textSize = DensityHelper.dp2px(context, size).toFloat()
        val bounds = Rect()
        paint.getTextBounds(text, 0, text.length, bounds)
        return drawTextToBitmap(
            bitmap,
            text,
            paint,
            (bitmap.width - bounds.width())/2f,
            (bitmap.height + bounds.height())/2f
        )
    }

    //endregion

    //region 私有方法
    /**
     * 设置水印图片在左上角
     */
    private fun createWaterMaskBitmap(
        src: Bitmap,
        watermark: Bitmap,
        paddingLeft: Float,
        paddingTop: Float
    ): Bitmap {
        val width = src.width
        val height = src.height
        // 创建一个新的和SRC长度宽度一样的位图
        val bitmapNew = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        //将该图片作为画布
        val canvas = Canvas(bitmapNew)
        //在画布(0,0)坐标上开始绘制原始图片
        canvas.drawBitmap(src, 0f, 0f, null)
        //在画布上绘制水印图片
        canvas.drawBitmap(watermark, paddingLeft, paddingTop, null)
        //保存
        canvas.save()
        //存储
        canvas.restore()
        return bitmapNew
    }

    /**
     * 在图片上绘制文字
     */
    private fun drawTextToBitmap(
        bitmap: Bitmap,
        text: String,
        paint: Paint,
        paddingLeft: Float,
        paddingTop: Float
    ): Bitmap {
        var bitmapConfig = bitmap.config
        //获取更清晰的图像采样
        paint.isDither = true
        //过滤一些
        paint.isFilterBitmap = true
        if (bitmapConfig == null) {
            bitmapConfig = Bitmap.Config.ARGB_8888
        }
        var bitmapNew = bitmap.copy(bitmapConfig, true)
        val canvas = Canvas(bitmapNew)
        canvas.drawText(text, paddingLeft, paddingTop, paint)
        return bitmapNew
    }

    //endregion
}