package com.io.commonlib.img;

//import com.luck.picture.lib.config.PictureConfig;
//import com.luck.picture.lib.config.PictureMimeType;
//import com.luck.picture.lib.language.LanguageConfig;
//
///**
// * Description:设置图片选择器的相关配置项
// * 2020-01-04 14:36
// * @author LiuMin
// */
//public final class PictureSelectorOptions {
//    //region 属性定义
//    /**
//     * 选择模式-单选，多选
//     */
//    private int selectionMode = PictureConfig.MULTIPLE;
//    /**
//     * 是否支持查看原图
//     */
//    private boolean isSupportOriginal = false;
//
//    /**
//     * 支持的语言-默认支持简体中文
//     */
//    private int language = LanguageConfig.CHINESE;
//    /**
//     * 选择要显示的文件类型
//     */
//    private int chooseMode = PictureMimeType.ofAll();
//    /**
//     * 图片选择的最大数量
//     */
//    private int maxSelectNum = 1;
//    /**
//     * 图片选择的最小数量
//     */
//    private int minSelectNum = 0;
//    /**
//     * 是否显示拍照按钮
//     */
//    private boolean isShowTakePhoto = true;
//    /**
//     * 每行显示的个数
//     */
//    private int imageSpanCount = 4;
//    /**
//     * 单选模式下，选完照片是否直接返回
//     */
//    private boolean isSingleDirectReturn = true;
//    /**
//     * 是否可播放音频
//     */
//    private boolean enablePreviewAudio = true;
//    /**
//     * 是否可预览图片
//     */
//    private boolean enablePreviewImage = true;
//    /**
//     * 是否可预览视频
//     */
//    private boolean enablePreviewVideo = true;
//    /**
//     * 图片列表点击 缩放效果 默认true
//     */
//    private boolean isZoomAnim = true;
//    /**
//     * 是否支持压缩
//     */
//    private boolean enableCrop = false;
//    /**
//     * 图片压缩质量
//     */
//    private int compressQuality = 100;
//    /**
//     * 是否显示gif
//     */
//    private boolean isShowGif = false;
//
//    /**
//     * 裁剪框是否可拖拽
//     */
//    private boolean freeStyleCropEnabled = true;
//    /**
//     * 是否圆形裁剪
//     */
//    private boolean isCircleDimmedLayer = false;
//    /**
//     * 是否显示裁剪矩形边框 圆形裁剪时建议设为false
//     */
//    private boolean isShowCropFrame = false;
//    /**
//     * 是否显示裁剪矩形网格 圆形裁剪时建议设为false
//     */
//    private boolean isShowCropGrid = false;
//    /**
//     * 最小压缩大小，小于这个数则不压缩，单位kb
//     */
//    private int minimumCompressSize = 100;
//    /**
//     * 裁剪比例-x
//     */
//    private int aspectRatioX = 1;
//    /**
//     * 裁剪比例-y
//     */
//    private int aspectRatioY = 1;
//
//    /**
//     * 是否支持压缩
//     */
//    private boolean enableCompress = false;
//    //endregion
//
//    //region get方法
//
//    public int getSelectionMode() {
//        return selectionMode;
//    }
//
//    public boolean isSupportOriginal() {
//        return isSupportOriginal;
//    }
//
//    public int getLanguage() {
//        return language;
//    }
//
//    public int getChooseMode() {
//        return chooseMode;
//    }
//
//    public int getMaxSelectNum() {
//        return maxSelectNum;
//    }
//
//    public int getMinSelectNum() {
//        return minSelectNum;
//    }
//
//    public boolean isShowTakePhoto() {
//        return isShowTakePhoto;
//    }
//
//    public int getImageSpanCount() {
//        return imageSpanCount;
//    }
//
//    public boolean isSingleDirectReturn() {
//        return isSingleDirectReturn;
//    }
//
//    public boolean isEnablePreviewAudio() {
//        return enablePreviewAudio;
//    }
//
//    public boolean isEnablePreviewImage() {
//        return enablePreviewImage;
//    }
//
//    public boolean isEnablePreviewVideo() {
//        return enablePreviewVideo;
//    }
//
//    public boolean isZoomAnim() {
//        return isZoomAnim;
//    }
//
//    public boolean isEnableCrop() {
//        return enableCrop;
//    }
//
//    public int getCompressQuality() {
//        return compressQuality;
//    }
//
//    public boolean isShowGif() {
//        return isShowGif;
//    }
//
//    public boolean isFreeStyleCropEnabled() {
//        return freeStyleCropEnabled;
//    }
//
//    public boolean isCircleDimmedLayer() {
//        return isCircleDimmedLayer;
//    }
//
//    public boolean isShowCropFrame() {
//        return isShowCropFrame;
//    }
//
//    public boolean isShowCropGrid() {
//        return isShowCropGrid;
//    }
//
//    public int getMinimumCompressSize() {
//        return minimumCompressSize;
//    }
//
//    public int getAspectRatioX() {
//        return aspectRatioX;
//    }
//
//    public int getAspectRatioY() {
//        return aspectRatioY;
//    }
//
//    public boolean isEnableCompress() {
//        return enableCompress;
//    }
//    //endregion
//
//    public static final class Builder{
//        private PictureSelectorOptions options = new PictureSelectorOptions();
//        public Builder() {
//        }
//
//        public PictureSelectorOptions build() {
//            return this.options;
//        }
//
//        public PictureSelectorOptions.Builder setSelectionMode(int selectionMode) {
//            this.options.selectionMode = selectionMode;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setSupportOriginal(boolean supportOriginal) {
//            this.options.isSupportOriginal = supportOriginal;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setLanguage(int language) {
//            this.options.language = language;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setChooseMode(int chooseMode) {
//            this.options.chooseMode = chooseMode;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setMaxSelectNum(int maxSelectNum) {
//            this.options.maxSelectNum = maxSelectNum;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setMinSelectNum(int minSelectNum) {
//            this.options.minSelectNum = minSelectNum;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setShowTakePhoto(boolean showTakePhoto) {
//            this.options.isShowTakePhoto = showTakePhoto;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setImageSpanCount(int imageSpanCount) {
//            this.options.imageSpanCount = imageSpanCount;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setSingleDirectReturn(boolean singleDirectReturn) {
//            this.options.isSingleDirectReturn = singleDirectReturn;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setEnablePreviewAudio(boolean enablePreviewAudio) {
//            this.options.enablePreviewAudio = enablePreviewAudio;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setEnablePreviewImage(boolean enablePreviewImage) {
//            this.options.enablePreviewImage = enablePreviewImage;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setEnablePreviewVideo(boolean enablePreviewVideo) {
//            this.options.enablePreviewVideo = enablePreviewVideo;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setZoomAnim(boolean zoomAnim) {
//            this.options.isZoomAnim = zoomAnim;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setEnableCrop(boolean enableCrop) {
//            this.options.enableCrop = enableCrop;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setCompressQuality(int compressQuality) {
//            this.options.compressQuality = compressQuality;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setShowGif(boolean showGif) {
//            this.options.isShowGif = showGif;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setFreeStyleCropEnabled(boolean freeStyleCropEnabled) {
//            this.options.freeStyleCropEnabled = freeStyleCropEnabled;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setCircleDimmedLayer(boolean circleDimmedLayer) {
//            this.options.isCircleDimmedLayer = circleDimmedLayer;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setShowCropFrame(boolean showCropFrame) {
//            this.options.isShowCropFrame = showCropFrame;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setShowCropGrid(boolean showCropGrid) {
//            this.options.isShowCropGrid = showCropGrid;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setMinimumCompressSize(int minimumCompressSize) {
//            this.options.minimumCompressSize = minimumCompressSize;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setAspectRatioX(int aspectRatioX) {
//            this.options.aspectRatioX = aspectRatioX;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setAspectRatioY(int aspectRatioY) {
//            this.options.aspectRatioY = aspectRatioY;
//            return this;
//        }
//
//        public PictureSelectorOptions.Builder setEnableCompress(boolean enableCompress) {
//            this.options.enableCompress = enableCompress;
//            return this;
//        }
//    }
//}
