package com.io.commonlib.enum

enum class NetworkType(val type: String) {
    Wifi("Wifi"),//Wifi
    Cellular("Cellular"),//移动网络
    Ethernet("Ethernet"),//热点
    VPN("VPN"),//vpn
    Null("Null"),//没有网络
}