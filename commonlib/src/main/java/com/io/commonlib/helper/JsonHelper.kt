package com.io.commonlib.helper

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Description:JSON操作工具类
 * 2021-03-02 17:08
 * @author LiuMin
 */
object JsonHelper {

    /**
     * 将指定对象转换为json字符串
     */
    fun <T> toJson(t: T?): String? {
        return GsonBuilder().disableHtmlEscaping().create().toJson(t)
    }

    /**
     * 将指定对象转换为json字符串
     */
    fun <T> toJson(t: List<T>?): String? {
        return GsonBuilder().disableHtmlEscaping().create().toJson(t)
    }

    fun <T> parseJsonToT(json: String?, clazz: Class<T>?): T? {
        val type = object : TypeToken<T>() {}.type
        json?.let {
            return Gson().fromJson<T>(json, type)
        }
        return null
    }

    /**
     * 将json字符串解析成指定类型的对象
     */
    fun <T> parseJsonToT(json: String?, type: Type): T? {
        json?.let {
            return Gson().fromJson<T>(json, type)
        }
        return null
    }

    /**
     * 从json字符串中解析出指定对象
     */
    fun <T> parseJson(json: String?, clazz: Class<T>?): T? {
        json?.let {
            clazz?.let { it0 ->
                return Gson().fromJson(it, it0)
            }
        }
        return null
    }

    /**
     * 从json字符串中解析出指定的对象数组
     */
    fun <T> parseJsonArray(json: String?, clazz: Class<T>?): List<T>? {
        json?.let {
            clazz?.let {
                val type = ListParameterizedType(clazz)
                return Gson().fromJson(json, type)
            }
        }
        return null
    }

    //region 下面这些方法可以选择放弃了
    /**
     * 从json字符串中读取指定key的value
     */
    @Deprecated(message = "建议直接用对象获取即可")
    fun readJsonAsString(json: String?, key: String): String {
        val element = JsonParser.parseString(json)
        val root = element.asJsonObject
        val result = root.get(key).asString
        return result
    }

    /**
     * 从json字符串中读取指定key的value
     */
    @Deprecated(message = "建议直接用对象获取即可")
    fun readJsonAsInt(json: String?, key: String): Int {
        val element = JsonParser.parseString(json)
        val root = element.asJsonObject
        val result = root.get(key).asInt
        return result
    }

    /**
     * 从json字符串中读取指定key的value
     */
    @Deprecated(message = "建议直接用对象获取即可")
    fun readJsonAsBoolean(json: String?, key: String): Boolean {
        val element = JsonParser.parseString(json)
        val root = element.asJsonObject
        val result = root.get(key).asBoolean
        return result
    }

    /**
     * 从json字符串中读取指定key的value
     */
    @Deprecated(message = "建议直接用对象获取即可")
    fun readJsonAsDouble(json: String?, key: String): Double {
        val element = JsonParser.parseString(json)
        val root = element.asJsonObject
        val result = root.get(key).asDouble
        return result
    }

    /**
     * 从json字符串中读取指定key的value
     */
    @Deprecated(message = "建议直接用对象获取即可")
    fun readJsonAsFloat(json: String?, key: String): Float {
        val element = JsonParser.parseString(json)
        val root = element.asJsonObject
        val result = root.get(key).asFloat
        return result
    }

    /**
     * 从json字符串中读取指定key的value
     */
    @Deprecated(message = "建议直接用对象获取即可")
    fun readJsonAsLong(json: String?, key: String): Long {
        val element = JsonParser.parseString(json)
        val root = element.asJsonObject
        val result = root.get(key).asLong
        return result
    }

    //endregion

    private class ListParameterizedType(private val type: Type) : ParameterizedType {
        override fun getRawType(): Type {
            return ArrayList::class.java
        }

        override fun getOwnerType(): Type? {
            return null
        }

        override fun getActualTypeArguments(): Array<Type> {
            return arrayOf(type)
        }

    }
}