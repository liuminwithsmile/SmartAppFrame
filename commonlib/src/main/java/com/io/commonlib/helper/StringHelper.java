package com.io.commonlib.helper;

import java.util.regex.Pattern;

/**
 * 字符串工具类
 * 2020/1/2 9:11
 *
 * @author LiuWeiHao
 * Email 872235631@qq.com
 */
public class StringHelper {
    /**
     * 是否空字符串,空格也属于空字符
     *
     * @param value 待比较字符串
     * @return boolean
     */
    public static boolean isEmpty(String value) {
        if (value == null) {
            return true;
        }
        return value.replaceAll(" ", "").length() == 0;
    }

    /**
     * 是否是邮箱号
     *
     * @param email 待比较字符串
     * @return true 邮箱；false 否;
     */
    public static boolean isEmail(String email) {
        if (email == null || "".equals(email)) {
            return false;
        } else {
            String regex = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            return email.matches(regex);
        }
    }

    /**
     * 是否是手机号
     *
     * @param phoneNumber 手机号字符串
     * @return true 手机号；false 不是;
     */
    public static boolean isPhoneNumber(String phoneNumber) {
        if (phoneNumber == null || "".equals(phoneNumber)) {
            return false;
        } else {
            String regex = "^1[3|4|5|6|7|8|9][0-9]\\d{8}$";
            return phoneNumber.matches(regex);
        }
    }

    /**
     * 是否全部是汉字
     *
     * @param str 字符串
     * @return true 全部是汉字;false 不是全部是汉字
     */
    public static boolean isChinese(String str) {
        return Pattern.compile("[\u0391-\uFFE5]+$").matcher(str).matches();
    }

    /*
     * 判断是否为整数
     * @param str 传入的字符串
     * @return 是整数返回true,否则返回false
     */

    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    /**
     * 判断是否为小数
     *
     * @param str 带比较字符串
     * @return true 小数；false非小数
     */
    public static boolean isDouble(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?\\d+\\.\\d+$");
        return pattern.matcher(str).matches();
    }

    /**
     * 去除页面的非法字符检查
     *
     * @param str 字符串
     * @return 过滤后的字符串
     */
    public static String replaceStr(String str) {
        if (str != null && str.length() > 0) {
            str = str.replaceAll("~", "");
            str = str.replaceAll(" ", "");
            str = str.replaceAll("　", "");
            str = str.replaceAll(" ", "");
            str = str.replaceAll("`", "");
            str = str.replaceAll("!", "");
            str = str.replaceAll("@", "");
            str = str.replaceAll("#", "");
            str = str.replaceAll("\\$", "");
            str = str.replaceAll("%", "");
            str = str.replaceAll("\\^", "");
            str = str.replaceAll("&", "");
            str = str.replaceAll("\\*", "");
            str = str.replaceAll("\\(", "");
            str = str.replaceAll("\\)", "");
            str = str.replaceAll("-", "");
            str = str.replaceAll("_", "");
            str = str.replaceAll("=", "");
            str = str.replaceAll("\\+", "");
            str = str.replaceAll("\\{", "");
            str = str.replaceAll("\\[", "");
            str = str.replaceAll("\\}", "");
            str = str.replaceAll("\\]", "");
            str = str.replaceAll("\\|", "");
            str = str.replaceAll("\\\\", "");
            str = str.replaceAll(";", "");
            str = str.replaceAll(":", "");
            str = str.replaceAll("'", "");
            str = str.replaceAll("\\\"", "");
            str = str.replaceAll("<", "");
            str = str.replaceAll(">", "");
            str = str.replaceAll("\\.", "");
            str = str.replaceAll("\\?", "");
            str = str.replaceAll("/", "");
            str = str.replaceAll("～", "");
            str = str.replaceAll("`", "");
            str = str.replaceAll("！", "");
            str = str.replaceAll("＠", "");
            str = str.replaceAll("＃", "");
            str = str.replaceAll("＄", "");
            str = str.replaceAll("％", "");
            str = str.replaceAll("︿", "");
            str = str.replaceAll("＆", "");
            str = str.replaceAll("×", "");
            str = str.replaceAll("（", "");
            str = str.replaceAll("）", "");
            str = str.replaceAll("－", "");
            str = str.replaceAll("＿", "");
            str = str.replaceAll("＋", "");
            str = str.replaceAll("＝", "");
            str = str.replaceAll("｛", "");
            str = str.replaceAll("［", "");
            str = str.replaceAll("｝", "");
            str = str.replaceAll("］", "");
            str = str.replaceAll("｜", "");
            str = str.replaceAll("＼", "");
            str = str.replaceAll("：", "");
            str = str.replaceAll("；", "");
            str = str.replaceAll("＂", "");
            str = str.replaceAll("＇", "");
            str = str.replaceAll("＜", "");
            str = str.replaceAll("，", "");
            str = str.replaceAll("＞", "");
            str = str.replaceAll("．", "");
            str = str.replaceAll("？", "");
            str = str.replaceAll("／", "");
            str = str.replaceAll("·", "");
            str = str.replaceAll("￥", "");
            str = str.replaceAll("……", "");
            str = str.replaceAll("（", "");
            str = str.replaceAll("）", "");
            str = str.replaceAll("——", "");
            str = str.replaceAll("-", "");
            str = str.replaceAll("【", "");
            str = str.replaceAll("】", "");
            str = str.replaceAll("、", "");
            str = str.replaceAll("”", "");
            str = str.replaceAll("’", "");
            str = str.replaceAll("《", "");
            str = str.replaceAll("》", "");
            str = str.replaceAll("“", "");
            str = str.replaceAll("。", "");
        }
        return str;
    }

}
