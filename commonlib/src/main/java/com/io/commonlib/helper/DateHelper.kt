package com.io.commonlib.helper

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * 日期工具类
 * 2020/1/2 10:58
 *
 * @author LiuWeiHao
 * Email 872235631@qq.com
 */
object DateHelper {
    /**
     * 获取当前时间戳
     *
     * @return long型的时间戳
     */
    val currentTimeUnix: Long
        get() = System.currentTimeMillis()

    /**
     * 获取指定日期前或后几天的时间
     *
     * @param day 天数  正数为此日期前的天数
     */
    fun getCurrentTimeUnixBefore(day: Int): Long {
        return System.currentTimeMillis() - 1000 * 60 * 60 * 24 * day
    }

    /**
     * 获取当天日期
     */
    val currentDateStr: String
        get() {
            val currentTime: String
            val date = Date()
            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            currentTime = dateFormat.format(date)
            return currentTime
        }

    /**
     * 获取当天日期
     */
    val currentDateAndMinuteStr: String
        get() {
            val currentTime: String
            val date = Date()
            val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
            currentTime = dateFormat.format(date)
            return currentTime
        }

    /**
     * 获取当前时间
     *
     * @return String yyyy-MM-dd HH:mm:ss
     */
    val currentDateAndTimeStr: String
        get() {
            val currentTime: String
            val date = Date()
            val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            currentTime = dateFormat.format(date)
            return currentTime
        }

    /**
     * 获取当前时间 String yyyy-MM-dd HH:mm:ss:SSS
     */
    fun currentDateAndTimeMsStr(): String {
        val date = Date()
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", Locale.getDefault())
        return dateFormat.format(date)
    }

    /**
     * 根据传入的格式获取当前时间
     */
    fun getNowDateTimeStr(df: String):String{
        val date = Date()
        val dateFormat = SimpleDateFormat(df, Locale.getDefault())
        return dateFormat.format(date)
    }

    /**
     * string类型日期转时间戳
     *
     * @param dateString 日期字符串
     * @return 时间错
     */
    fun formatStringToDateUnix(dateString: String?): String {
        if (dateString == null || dateString.length == 0) {
            return ""
        }
        if (" 00:00:00:000" == dateString || " 23:59:59:999" == dateString || " 23:59:59:000" == dateString) {
            return ""
        }
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val date: Date
        return try {
            date = dateFormat.parse(dateString)
            date.time.toString()
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    /**
     * 日期格式转化
     *
     * @param dateString 字符串日期
     * @return 指定格式后的日期
     */
    fun formatStringToStr(dateString: String?): String? {
        var dateString = dateString
        if (dateString == null || dateString.length == 0) {
            return ""
        }
        if (" 00:00:00:000" == dateString || " 23:59:59:999" == dateString || " 23:59:59:000" == dateString) {
            return ""
        }
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val date: Date?
        return try {
            date = dateFormat.parse(dateString)
            val format = SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒", Locale.getDefault())
            if (date != null) {
                dateString = format.format(date)
            }
            dateString
        } catch (e: Exception) {
            e.printStackTrace()
            dateString
        }
    }

    fun formatStringToDate(dateString: String?): Date? {
        if (dateString == null || dateString.length == 0) {
            return null
        }
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val date: Date
        return try {
            date = dateFormat.parse(dateString)
            date
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    /**
     * Date日期格式化为String类型
     *
     * @param date Date
     * @return String
     */
    fun formatDate(date: Date?): String {
        val dateString: String
        val dateFormat = SimpleDateFormat("yyyy-MM-dd ", Locale.getDefault())
        dateString = dateFormat.format(date)
        return dateString
    }

    /**
     * Date日期格式化为String类型
     *
     * @param date Date
     * @return String
     */
    fun formatLongDate(date: Date): String {
        val dateString: String
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        dateString = dateFormat.format(date)
        return dateString
    }

    /**
     * 格式化时间戳Unix
     *
     * @param unix Unix时间戳
     * @return String yyyy-MM-dd HH:mm:ss
     */
    fun formatUnixToString(unix: String?): String {
        if (unix == null || unix.length == 0) {
            return ""
        }
        val dateString: String
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        dateString = dateFormat.format(Date(unix.toLong()))
        return dateString
    }

    /**
     * 格式化时间戳Unix
     *
     * @param unix Unix时间戳
     * @return String yyyy-MM-dd
     */
    fun formatUnixToStringDate(unix: String?): String {
        if (unix == null || unix.length == 0) {
            return ""
        }
        val dateString: String
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateString = dateFormat.format(Date(unix.toLong()))
        return dateString
    }

    /**
     * 格式化时间戳Unix
     *
     * @param unix Unix时间戳
     * @return String yyyy-MM-dd
     */
    fun formatUnixToStringDate(unix: Long): String {
        val dateString: String
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateString = dateFormat.format(Date(unix))
        return dateString
    }

    /**
     * 格式化时间 时间戳转date
     */
    fun formatLongDate(date: Long): String {
        return if (date == 0L) "" else formatLongDate(Date(date))
    }

    /**
     * 计算日期差
     *
     * @param startDateString 开始日期
     * @param endDateString   结束日期
     * @return 天数=后日期-前日期
     */
    fun getDateBetween(startDateString: String?, endDateString: String?): Long {
        var days: Long = 0
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        try {
            val startDate = dateFormat.parse(startDateString)
            val endDate = dateFormat.parse(endDateString)
            val diff = endDate.time - startDate.time
            days = diff / (1000 * 60 * 60 * 24)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return days
    }

    /**
     * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
     *
     * @param nowTime   当前时间
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     * @author jqlin
     */
    fun isEffectiveDate(nowTime: String, startTime: String, endTime: String): Boolean {
        val format = "HH:mm"
        if (nowTime == startTime || nowTime == endTime) {
            return true
        }
        try {
            val nowTimeDate = SimpleDateFormat(format).parse(nowTime)
            val date = Calendar.getInstance()
            date.time = nowTimeDate
            val startDateTime = SimpleDateFormat(format).parse(startTime)
            val begin = Calendar.getInstance()
            begin.time = startDateTime
            val endDateTime = SimpleDateFormat(format).parse(endTime)
            val end = Calendar.getInstance()
            end.time = endDateTime
            return date.after(begin) && date.before(end)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return false
    }

    val curTimeStr: String
        get() {
            val currentTime: String
            val date = Date()
            val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
            currentTime = dateFormat.format(date)
            return currentTime
        }
}