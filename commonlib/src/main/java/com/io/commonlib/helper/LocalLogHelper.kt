package com.io.commonlib.helper

import com.elvishew.xlog.LogConfiguration
import com.elvishew.xlog.LogLevel
import com.elvishew.xlog.XLog
import com.elvishew.xlog.interceptor.BlacklistTagsFilterInterceptor
import com.elvishew.xlog.printer.AndroidPrinter
import com.elvishew.xlog.printer.ConsolePrinter
import com.elvishew.xlog.printer.Printer
import com.elvishew.xlog.printer.file.FilePrinter
import com.elvishew.xlog.printer.file.backup.NeverBackupStrategy
import com.elvishew.xlog.printer.file.clean.FileLastModifiedCleanStrategy
import com.elvishew.xlog.printer.file.naming.DateFileNameGenerator

/**
 * 本地日志工具类
 */
object LocalLogHelper {
    private var myLogLevel: Int = 0

    /**
     * 初始化
     */
    fun initLog(localPath: String, packageName: String, maxTime:Long, logLevel: Int) {
        myLogLevel = logLevel
        LogLevel.ALL
        val config: LogConfiguration = LogConfiguration.Builder()
            .logLevel(myLogLevel).tag(packageName)
            .build()

        val androidPrinter: Printer = AndroidPrinter()

        val consolePrinter: Printer = ConsolePrinter()
        val filePrinter: Printer =
            FilePrinter.Builder(localPath) // Specify the path to save log file
                .fileNameGenerator(DateFileNameGenerator()) // Default: ChangelessFileNameGenerator("log")
                .backupStrategy(NeverBackupStrategy()) // Default: FileSizeBackupStrategy(1024 * 1024)
                .cleanStrategy(FileLastModifiedCleanStrategy(maxTime)) // Default: NeverCleanStrategy()
                //.flattener(MyFlattener()) // Default: DefaultFlattener
                .build()

        XLog.init( // Initialize XLog
            config,  // Specify the log configuration, if not specified, will use new LogConfiguration.Builder().build()
            androidPrinter,  // Specify printers, if no printer is specified, AndroidPrinter(for Android)/ConsolePrinter(for java) will be used.
            consolePrinter,
            filePrinter
        )
    }

    fun v(tag: String, info: String){
        if(myLogLevel >= LogLevel.VERBOSE){
            XLog.v("${tag}: ${info}")
        }
    }

    fun d(tag: String, info: String){
        if(myLogLevel >= LogLevel.DEBUG){
            XLog.d("${tag}: ${info}")
        }
    }

    fun i(tag: String, info: String){
        if(myLogLevel >= LogLevel.INFO){
            XLog.d("${tag}: ${info}")
        }
    }

    fun w(tag: String, info: String){
        if(myLogLevel >= LogLevel.WARN){
            XLog.e("${tag}: ${info}")
        }
    }

    fun e(tag: String, info: String){
        if(myLogLevel >= LogLevel.ERROR){
            XLog.e("${tag}: ${info}")
        }
    }
}