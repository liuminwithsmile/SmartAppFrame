package com.io.commonlib.helper

import android.util.Log

/**
 * Description:日志工具类
 * 可以通过设置全局日志级别，控制输出哪些级别的日志
 * 当日志级别大于0的时候，除了debug级别其他的全部输出，
 * 0->debug + info + warn + error, 1->info + warn + error, 2->warn + error, 3->error
 * 2020-07-29 14:02
 * @author LiuMin
 */
object LogHelper {

    //Debug级别的日志只能用于开发和测试环境中，不可以用于生产环境
    private val LOG_DEBUG = 0

    //这个级别的日志可以记录一些信息型消息比如服务器启动成功、输入的数据、输出的数据等
    private val LOG_INFO = 1

    //用来记录警告信息比如客户端和服务器之间的连接中断、数据库连接丢失、Socket达到上限
    private val LOG_WARN = 2

    //用于记录error和Exception
    private val LOG_ERROR = 3

    var log_level = LOG_DEBUG

    fun d(tag: String, msg: String) {
        if (log_level == LOG_DEBUG) {
            Log.d(tag, msg)
        }
    }

    fun i(tag: String, msg: String) {
        if (log_level <= LOG_INFO) {
            Log.i(tag, msg)
        }
    }

    fun w(tag: String, msg: String) {
        if (log_level <= LOG_WARN) {
            Log.w(tag, msg)
        }
    }

    fun e(tag: String, msg: String) {
        if (log_level <= LOG_ERROR) {
            Log.e(tag, msg)
        }
    }

}