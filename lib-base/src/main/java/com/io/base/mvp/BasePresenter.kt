package com.io.base.mvp

/**
 * Description:基类 Presenter
 * 2021-03-10 10:27
 * @author LiuMin
 */
open class BasePresenter<M : IModel, V : IView>() : IPresenter {
    protected var mModel: M? = null
    protected var mView: V? = null

    init {
        onStart()
    }

    constructor(view: V) : this() {
        this.mView = view
        onStart()
    }
    constructor(model: M, view: V) : this() {
        this.mModel = model
        this.mView = view
        onStart()
    }

    override fun onStart() {

    }

    override fun onDestroy() {
        mModel?.let {
            it.onDestroy()
        }
        mModel = null
        mView = null
    }
}