package com.io.base.mvp

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

/**
 * Description:对RecyclerView.ViewHolder的封装
 * 2021-03-09 10:40
 * @author LiuMin
 */
open class BaseHolder(viewBinding: ViewBinding) : RecyclerView.ViewHolder(viewBinding.root) {

    protected var mOnItemViewClickListener: OnItemViewClickListener? = null
    protected var mOnItemViewLongClickListener: OnItemViewLongClickListener? = null

    var mViewBinding: ViewBinding

    init {
        mViewBinding = viewBinding
    }

    fun addOnItemClickListener(view: View) {
        view.setOnClickListener {
            mOnItemViewClickListener?.onItemClick(view, layoutPosition)
        }
    }

    fun addOnLongClickListener(view: View) {
        view.setOnLongClickListener{
            mOnItemViewLongClickListener?.onItemLongClick(view, layoutPosition)
            true
        }
    }

    fun setOnItemClickListener(listener: OnItemViewClickListener){
        this.mOnItemViewClickListener = listener
    }

    fun setOnItemViewLongClickListener(listener: OnItemViewLongClickListener){
        this.mOnItemViewLongClickListener = listener
    }

    fun onRelease(){}

    interface OnItemViewClickListener {
        /**
         * item 被点击
         *
         * @param view     被点击的 {@link View}
         * @param position 在 RecyclerView 中的位置
         */
        fun onItemClick(view: View, position: Int)
    }

    interface OnItemViewLongClickListener {
        /**
         * item 被长按
         *
         * @param view     被点击的 {@link View}
         * @param position 在 RecyclerView 中的位置
         */
        fun onItemLongClick(view: View, position: Int)
    }
}