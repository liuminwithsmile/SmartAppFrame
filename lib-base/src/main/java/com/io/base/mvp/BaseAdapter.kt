package com.io.base.mvp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

/**
 * Description:Adapter的基类
 * 2021-03-09 14:12
 * @author LiuMin
 */
abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseHolder>() {

    protected lateinit var mInfos: MutableList<T>
    protected var mOnRecyclerViewItemClickListener: OnRecyclerViewItemClickListener<T>? = null
    protected var mOnRecyclerViewItemLongClickListener: OnRecyclerViewItemLongClickListener<T>? =
        null

    init {
        mInfos = ArrayList<T>()
    }

    /**
     * 返回数据总个数
     *
     * @return 数据总个数
     */
    override fun getItemCount(): Int {
        return mInfos.size
    }

    /**
     * 返回数据集合
     *
     * @return 数据集合
     */
    open fun getInfos(): MutableList<T>? {
        return mInfos
    }

    /**
     * 添加数据
     */
    open fun notifyData(list: List<T>?) {
        if (list != null) {
            mInfos.clear()
            mInfos.addAll(list)
            notifyDataSetChanged()
        } else {
            mInfos.clear()
            notifyDataSetChanged()
        }
    }

    open fun append(response: List<T>?) {
        if (response != null) {
            mInfos.addAll(response)
        }
        notifyDataSetChanged()
    }

    /**
     * 根据索引移除对象
     *
     * @param index 索引
     */
    open fun remove(index: Int) {
        mInfos.removeAt(index)
        notifyDataSetChanged()
    }

    /**
     * 移除指定对象
     *
     * @param object 对象
     */
    open fun remove(obj: T) {
        mInfos.remove(obj)
        notifyDataSetChanged()
    }

    /**
     * 清空列表
     */
    open fun clear() {
        mInfos.clear()
        notifyDataSetChanged()
    }

    /**
     * 获得 RecyclerView 中某个 position 上的 item 数据
     *
     * @param position 在 RecyclerView 中的位置
     * @return 数据
     */
    fun getItem(position: Int): T {
        return mInfos[position]
    }

    /**
     * 遍历所有 {@link BaseHolder}, 释放他们需要释放的资源
     *
     * @param recyclerView {@link RecyclerView}
     */
    fun releaseAllHolder(recyclerView: RecyclerView) {
        for (i in recyclerView.childCount - 1 downTo 0) {
            val view = recyclerView.getChildAt(i)
            val viewHolder = recyclerView.getChildViewHolder(view)
            if (viewHolder is BaseHolder) {
                viewHolder.onRelease()
            }
        }
    }

    /**
     * 设置 item 点击事件
     *
     * @param listener
     */
    open fun setOnItemClickListener(listener: OnRecyclerViewItemClickListener<T>) {
        this.mOnRecyclerViewItemClickListener = listener
    }

    /**
     * 设置 item 点击事件
     *
     * @param listener
     */
    open fun setOnItemLongClickListener(listener: OnRecyclerViewItemLongClickListener<T>?) {
        mOnRecyclerViewItemLongClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder {
        val viewBinding = getViewBinding(LayoutInflater.from(parent.context), parent, viewType)
        return BaseHolder(viewBinding)
    }

    /**
     * 绑定数据
     */
    override fun onBindViewHolder(holder: BaseHolder, position: Int) {
        setData(holder, holder.mViewBinding, mInfos.get(position), position)
        holder.setOnItemClickListener(object : BaseHolder.OnItemViewClickListener {
            override fun onItemClick(view: View, position: Int) {
                mOnRecyclerViewItemClickListener?.onItemClick(
                    view,
                    holder.mViewBinding,
                    mInfos[position],
                    position
                )
            }
        })
        holder.addOnItemClickListener(holder.mViewBinding.root)
        holder.setOnItemViewLongClickListener(object: BaseHolder.OnItemViewLongClickListener{
            override fun onItemLongClick(view: View, position: Int) {
                mOnRecyclerViewItemLongClickListener?.onLongClick(view, mInfos[position], position)
            }
        })
        holder.addOnLongClickListener(holder.mViewBinding.root)
    }

    protected abstract fun getViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ViewBinding

    /**
     * 设置数据
     *
     * @param data     数据
     * @param position 在 RecyclerView 中的位置
     */
    protected abstract fun setData(
        holder: BaseHolder,
        viewBinding: ViewBinding,
        data: T,
        position: Int
    )

    interface OnRecyclerViewItemClickListener<T> {
        /**
         * item 被点击
         *
         * @param view     被点击的 [View]
         * @param viewType 布局类型
         * @param data     数据
         * @param position 在 RecyclerView 中的位置
         */
        fun onItemClick(
            view: View,
            viewBinding: ViewBinding?,
            data: T,
            position: Int
        )
    }

    interface OnRecyclerViewItemLongClickListener<T> {
        /**
         * 长按
         * @param view
         * @param position
         */
        fun onLongClick(view: View?, data: T, position: Int)
    }
}