package com.io.base.mvp

import android.os.Bundle

/**
 * Description:框架中每个Activity需要实现的接口
 * 2021-03-09 9:25
 * @author LiuMin
 */
interface IActivity<VB> {
    //返回当前页面的ViewBinding对象
    fun initView(savedInstanceState: Bundle?):VB
    //页面初始化时需要加载的数据
    fun initData(savedInstanceState: Bundle?)
}