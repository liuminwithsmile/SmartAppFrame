package com.io.base.mvp

/**
 * Description:框架要求框架中的每个 Presenter 都需要实现此接口,以满足规范
 * 2021-03-10 10:30
 * @author LiuMin
 */
interface IPresenter {
    /**
     * 做一些初始化操作
     */
    fun onStart()
    /**
     * 在框架中 {@link Activity#onDestroy()} 时会默认调用 {@link IPresenter#onDestroy()}
     */
    fun onDestroy()
}