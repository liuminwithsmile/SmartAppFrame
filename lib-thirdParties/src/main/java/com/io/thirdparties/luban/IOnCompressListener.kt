package com.io.thirdparties.luban

import top.zibin.luban.OnCompressListener

/**
 * 此处只做封装不做任何处理
 * OnCompressListener--图片压缩后的回调接口
 */
interface IOnCompressListener: OnCompressListener {
}