package com.io.thirdparties.luban

import android.content.Context
import com.io.commonlib.helper.LogHelper
import top.zibin.luban.Luban
import top.zibin.luban.OnCompressListener

/**
 * 封装Luban图片压缩工具类
 */
object LubanHelper {
    private val TAG = LubanHelper::class.java.simpleName
    private var MIN_SIZE_LUBAN = 10 //低于10KB的图片不再进行压缩

    /**
     * 设置低于[size]KB的图片不再进行压缩
     */
    fun setMinSizeLuban(size: Int) {
        if(size > 0 && MIN_SIZE_LUBAN != size){
            MIN_SIZE_LUBAN = size
        }
    }

    /**
     * 通过鲁班先对图片进行压缩
     */
    fun compressImgByLuban(ctx: Context, localImgPath: String, listener: IOnCompressListener) {
        val parentPath = localImgPath.substring(0, localImgPath.lastIndexOf('/'))
        LogHelper.d(TAG, "compressImgByLuban--parentPath:$parentPath")
        Luban.with(ctx)
                ?.load(localImgPath)
                ?.ignoreBy(MIN_SIZE_LUBAN)
                ?.setTargetDir(parentPath)
                ?.setCompressListener(listener)
                ?.launch()
    }
}