package com.io.zxing

/**
 * Description:重新定义二维码的容错级别
 * 2021-05-12 11:13
 * @author LiuMin
 */
enum class ZXingErrorCorrectionLevel {
    L, M, Q, H
}