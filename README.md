# SmartAppFrame
android 框架


### 模块介绍
- app:壳，宿主app
- lib-base:公共sdk
- lib-thirdParties:三方库
    - Luban图片压缩
    - 阿里云oss文件上传    
- commoncontrol:控件封装
    - IconButton：可以使用文字图标的按钮
    - IconTextView：可以使用文字图标的文本控件
    - Banner：轮播图控件
    - IjkPlayer：封装ijkplayer播放器
    - BaseDialog：自定义dialog样式
    - ZoomImageView：可缩放图片
    - LargeImageView：加载大图控件
    - CommonPopupWindow：自定义PopupWindow
    - EditTextView：有边框的输入框
- commonlink：通信工具封装
    - OkHttp的封装
    - MQTT的封装
    - 串口工具的封装
- commonlib：公共类库封装    
    - AppInfoHelper：获取应用相关的信息
    - CommonDeviceHelper：获取设备相关的信息
    - DataHelper：数据类型转换工具类
    - DateHelper：日期时间工具类
    - EncryptHelper：数据签名工具类
    - FileHelper：文件操作工具类
    - LocalLogHelper：本地日志工具类
    - LogHelper：日志工具类
    - ShellHelper：脚本执行工具类
    - ToastHelper：Toast工具类
    - PinYinHelper：拼音工具
    - StringHelper：字符串工具类
    - DensityHelper：屏幕参数工具类
    
### V1.8.3 主要升级内容：
   - [x] 解决mqtt因为加密方法修改导致的无法注册成功的问题。
    
### V1.6.5 主要升级内容：
   - [x] 在串口工具类中添加发送数据的相关日志
### V1.6.4 主要升级内容：
- [x] 添加StringHelper、Banner、IjkPlayer、BaseDialog、ZoomImageView、LargeImageView
- [x] 修改串口工具类，在收到数据的回调中添加串口名称
 
### V1.6.3 主要升级内容：
- [x] 添加阿里云oss图片上传工具类；
- [x] 添加luban图片压缩工具类；
