package com.io.commonlink.serialport

import com.io.commonlib.helper.DataHelper
import com.io.commonlib.helper.LogHelper
import com.io.commonlib.helper.LogHelper.d
import com.kongqw.serialportlibrary.SerialPortManager
import com.kongqw.serialportlibrary.listener.OnOpenSerialPortListener
import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener
import java.io.File

class SerialPortHelper {
    private val TAG = SerialPortHelper::class.java.simpleName
    var state = false
    var mReceivedDataSerialPortListener: ReceivedDataSerialPortListener? = null
    var mPortNum = ""
    var mBandrate = 9600
    var mDataBits = 8
    var mStopBits = 1
    var mParity = 'N'
    var mSerialPortManager: SerialPortManager? = null

    init {
        if (mSerialPortManager == null) {
            mSerialPortManager = SerialPortManager()
        }
    }

    /**
     * 初始化时传入端口号和波特率
     * @param portNum
     * @param bandrate
     */
    fun initSerialPort(
        portNum: String, bandrate: Int,
        receivedDataSerialPortListener: ReceivedDataSerialPortListener?
    ) {
        mPortNum = portNum
        mBandrate = bandrate
        mReceivedDataSerialPortListener = receivedDataSerialPortListener
    }

    /**
     * 初始化时传入端口号和波特率
     * @param portNum
     * @param bandrate
     */
    fun initSerialPortWithInfo(
        portNum: String, bandrate: Int, dataBits: Int, stopBits: Int, parity: Char,
        receivedDataSerialPortListener: ReceivedDataSerialPortListener?
    ) {
        mPortNum = portNum
        mBandrate = bandrate
        mDataBits = dataBits
        mStopBits = stopBits
        mParity = parity
        mReceivedDataSerialPortListener = receivedDataSerialPortListener
    }

    /**
     * 打开串口
     */
    fun openSerialPort() {
        if (mSerialPortManager == null) {
            mSerialPortManager = SerialPortManager()
        }
        mSerialPortManager?.let {
            it.setOnOpenSerialPortListener(object :
                OnOpenSerialPortListener {
                override fun onSuccess(file: File) {
                    d(TAG, "串口打开成功:${mPortNum}")
                    state = true
                }

                override fun onFail(
                    file: File,
                    status: OnOpenSerialPortListener.Status
                ) {
                    d(TAG, "串口打开失败:${mPortNum}")
                    state = false
                }
            }).setOnSerialPortDataListener(MyReceivedDataSerialPortListener(mReceivedDataSerialPortListener, mPortNum))
            it.openSerialPort(
                File(mPortNum),
                mBandrate,
                mDataBits,
                mStopBits,
                mParity
            )
        }

    }

    /**
     * 关闭串口
     */
    fun closeSerialPort() {
        d(TAG, "串口关闭")
        if (mSerialPortManager != null) {
            mSerialPortManager!!.closeSerialPort()
            state = false
            mSerialPortManager = null
            mPortNum = ""
        }
    }

    /**
     * 发送数据
     * @param data
     */
    fun sendData(data: ByteArray?) {
        if (mSerialPortManager != null && state) {
            mSerialPortManager!!.sendBytes(data)
        }
    }

    interface ReceivedDataSerialPortListener {
        fun onReceivedDataSerialPortListener(data: ByteArray?, port:String)
    }

    class MyReceivedDataSerialPortListener(listener: ReceivedDataSerialPortListener?, port:String): OnSerialPortDataListener{
        private var mListener: ReceivedDataSerialPortListener?
        private var mPort:String

        init {
            this.mListener = listener
            this.mPort = port
        }

        override fun onDataReceived(p0: ByteArray?) {
            mListener?.onReceivedDataSerialPortListener(p0, mPort)
        }

        override fun onDataSent(p0: ByteArray?) {
            p0?.let {
                val dataStr = DataHelper().bytesToHex(p0)
                LogHelper.d("SerialPortHelper", "onDataSent--发送数据:${dataStr}")
            }?:let {
                LogHelper.d("SerialPortHelper", "onDataSent--发送数据为空")
            }
        }

    }
}